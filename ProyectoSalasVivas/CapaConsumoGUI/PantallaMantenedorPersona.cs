﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDTO;
using CapaNegocio;
using System.Windows.Forms;

namespace CapaConsumoGUI
{
    public partial class PantallaMantenedorPersona : Form
    {
        public PantallaMantenedorPersona()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void limpiar()
        {
            this.txtRutPersona.Text = String.Empty;
            this.txtNombrePersona.Text = String.Empty;
            this.txtApellidoPersona.Text = String.Empty;
            this.txtEmailPersona.Text = String.Empty;
            this.txtTelefonoPersona.Text = String.Empty;
            this.txtJornadaPersona.Text = String.Empty;
            this.cbxCarrera.DataSource = null; 
            this.cbxCategoria.DataSource = null;
            this.cbxCarrera.Text = String.Empty;
            this.cbxCategoria.Text = String.Empty;
            this.rbHombrePersona.Checked = false;
            this.rbMujerPersona.Checked = false;
            this.rbOtroPersona.Checked = false;
            this.txtRutPersona.Focus();
            this.dataGridViewPersona.DataSource = null;
        }

        private void reservaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionReservaSala pReserva = new PantallaTransaccionReservaSala();
            pReserva.ShowDialog();
        }

        private void salasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorSala pSala = new PantallaMantenedorSala();
            pSala.ShowDialog();
        }

        private void personaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorPersona pPersona = new PantallaMantenedorPersona();
            pPersona.ShowDialog();
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaConsultas pConsultas = new PantallaConsultas();
            pConsultas.ShowDialog();
        }

        private void equipoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionEquipo pEquipo = new PantallaTransaccionEquipo();
            pEquipo.ShowDialog();
        }

        private void PantallaMantenedorPersona_Load(object sender, EventArgs e)
        {
            NegocioPersona auxNegocioPersona = new NegocioPersona();

            this.cbxCategoria.DisplayMember = "nombre_categoria";
            this.cbxCategoria.ValueMember = "id_categoria";
            this.cbxCategoria.DataSource = auxNegocioPersona.MostrarNombreCategoria();

            this.cbxCarrera.DisplayMember = "nombre_carrera";
            this.cbxCarrera.ValueMember = "id_carrera";
            this.cbxCarrera.DataSource = auxNegocioPersona.MostrarNombreCarrera();

        }

        private void btnAgregarCarrera_Click(object sender, EventArgs e)
        {
            PantallaMantenedorCarrera pCarrera = new PantallaMantenedorCarrera();
            pCarrera.ShowDialog();
        }

        private void btnAgregarCategoria_Click(object sender, EventArgs e)
        {
            PantallaMantenedorCategoria pCategoria= new PantallaMantenedorCategoria();
            pCategoria.ShowDialog();
        }

        private void btnCrearPersona_Click(object sender, EventArgs e)
        {
            if (this.txtRutPersona.Text == "" && this.txtNombrePersona.Text == "" && this.txtApellidoPersona.Text == "" && this.txtEmailPersona.Text == "" && this.txtTelefonoPersona.Text == "" && this.txtJornadaPersona.Text == "")
            {
                MessageBox.Show("Complete los datos solicitados");
            }
            else
            {
                if (Convert.ToInt32(this.txtTelefonoPersona.Text) < 0)
                {
                    MessageBox.Show("El Campo telefono no permite el ingreso de numero negativos");
                }
                else
                {
                    ServicesPersona.WebServicePersonaSoapClient auxNegocioPersona = new ServicesPersona.WebServicePersonaSoapClient();
                    ServicesPersona.Persona auxPersona = new ServicesPersona.Persona();

                    auxPersona.Rut_persona = (this.txtRutPersona.Text);
                    auxPersona.Nombre_persona = this.txtNombrePersona.Text;
                    auxPersona.Apellido_persona = (this.txtApellidoPersona.Text);

                    if (this.rbHombrePersona.Checked)
                    {
                        auxPersona.Sexo = "Hombre";
                    }
                    else if (this.rbMujerPersona.Checked)
                    {
                        auxPersona.Sexo = "Mujer";
                    }
                    else
                    {
                        auxPersona.Sexo = "Otro";
                    }

                    auxPersona.Telefono = Convert.ToInt32(this.txtTelefonoPersona.Text);
                    auxPersona.Email = this.txtEmailPersona.Text;
                    auxPersona.Id_carrera = Convert.ToInt32(this.cbxCarrera.SelectedValue);
                    auxPersona.Id_categoria = Convert.ToInt32(this.cbxCategoria.SelectedValue);
                    auxPersona.Jornada = this.txtJornadaPersona.Text;
                    auxNegocioPersona.grabarPersonaService(auxPersona);
                    MessageBox.Show("Datos Guardados", "Sistema");
                    this.limpiar();
                }

            }        
        }

        private void btnListarPersona_Click(object sender, EventArgs e)
        {
            if (this.btnListarPersona.Text.Equals("Limpiar"))
            {
                limpiar();
                this.btnListarPersona.Text = "Listar";
            }
            else
            {
                ServicesPersona.WebServicePersonaSoapClient auxNegocioPersona = new ServicesPersona.WebServicePersonaSoapClient();
                
                this.dataGridViewPersona.DataSource = auxNegocioPersona.listarPersonaService();
                this.dataGridViewPersona.DataMember = "Persona";
            }
                

        }

        private void btnEliminarPersona_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtRutPersona.Text == "")
                {
                    MessageBox.Show("Ingrese un rut");
                }
                else
                {
                    ServicesPersona.WebServicePersonaSoapClient auxNegocioPersona = new ServicesPersona.WebServicePersonaSoapClient();
                    ServicesPersona.Persona auxPersona = new ServicesPersona.Persona();


                    if (auxNegocioPersona.buscarPersona(this.txtRutPersona.Text).Rut_persona.Equals(0))
                    {
                        MessageBox.Show("El rut no existe");
                    }
                    else
                    {
                        if (MessageBox.Show("Estas seguro de eliminar ", "sistema", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            

                            auxNegocioPersona.eliminarPersonaService(this.txtRutPersona.Text);
                            MessageBox.Show("Persona eliminada exitosamente", "Sistema");
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            
        }

        private void btnEditarPersona_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtRutPersona.Text == "")
                {
                    MessageBox.Show("Ingrese un rut");
                }
                else
                {
                    if (this.btnEditarPersona.Text.Equals("Buscar"))
                    {
                        ServicesPersona.WebServicePersonaSoapClient auxNegocioPersona = new ServicesPersona.WebServicePersonaSoapClient();
                        ServicesPersona.Persona auxPersona = new ServicesPersona.Persona();

                        if (auxNegocioPersona.buscarPersona(this.txtRutPersona.Text).Rut_persona.Equals(0))
                        {
                            MessageBox.Show("El rut ingresado no existe");
                        }
                        else
                        {
                            auxPersona = auxNegocioPersona.buscarPersona(this.txtRutPersona.Text);

                            this.txtRutPersona.Text = auxPersona.Rut_persona;
                            this.txtNombrePersona.Text = auxPersona.Nombre_persona;
                            this.txtApellidoPersona.Text = auxPersona.Apellido_persona;
                            this.txtTelefonoPersona.Text = Convert.ToString(auxPersona.Telefono);
                            this.txtEmailPersona.Text = auxPersona.Email;
                            this.cbxCarrera.SelectedValue = auxPersona.Id_carrera;
                            this.cbxCategoria.SelectedValue = auxPersona.Id_categoria;
                            this.txtJornadaPersona.Text = auxPersona.Jornada;
                            this.btnEditarPersona.Text = "Editar";
                        }

                    }
                    else
                    {
                        if (this.btnEditarPersona.Text.Equals("Editar"))
                        {
                            if (MessageBox.Show("Estas seguro de actualizar", "sistema", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                ServicesPersona.WebServicePersonaSoapClient auxNegocioPersona = new ServicesPersona.WebServicePersonaSoapClient();
                                ServicesPersona.Persona auxPersona = new ServicesPersona.Persona();

                                auxPersona.Rut_persona = (this.txtRutPersona.Text);
                                auxPersona.Nombre_persona = this.txtNombrePersona.Text;
                                auxPersona.Apellido_persona = (this.txtApellidoPersona.Text);

                                if (this.rbHombrePersona.Checked)
                                {
                                    auxPersona.Sexo = "Hombre";
                                }
                                else if (this.rbMujerPersona.Checked)
                                {
                                    auxPersona.Sexo = "Mujer";
                                }
                                else
                                {
                                    auxPersona.Sexo = "Otro";
                                }

                                auxPersona.Telefono = Convert.ToInt32(this.txtTelefonoPersona.Text);
                                auxPersona.Email = this.txtEmailPersona.Text;
                                auxPersona.Id_carrera = Convert.ToInt32(this.cbxCarrera.SelectedValue);
                                auxPersona.Id_categoria = Convert.ToInt32(this.cbxCategoria.SelectedValue);
                                auxPersona.Jornada = this.txtJornadaPersona.Text;
                                auxNegocioPersona.actualizarPersonaService(auxPersona);
                                MessageBox.Show("Datos Actualizados", "Sistema");
                                this.limpiar();

                            }
                        }   
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
            
                
        }
    }
}
