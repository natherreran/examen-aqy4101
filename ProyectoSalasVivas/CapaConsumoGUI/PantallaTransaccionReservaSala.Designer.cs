﻿namespace CapaConsumoGUI
{
    partial class PantallaTransaccionReservaSala
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PantallaTransaccionReservaSala));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dataGridViewReserva = new System.Windows.Forms.DataGridView();
            this.btoCrear = new System.Windows.Forms.Button();
            this.txtCodigoReserva = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reservaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.txtHrInicio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimePickerFecha = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtComentarios = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btoRenovar = new System.Windows.Forms.Button();
            this.btoAnular = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMinInicio = new System.Windows.Forms.TextBox();
            this.txtMinTermino = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtHrTermino = new System.Windows.Forms.TextBox();
            this.cbxCodigoSala = new System.Windows.Forms.ComboBox();
            this.cbxCodigoEquipo = new System.Windows.Forms.ComboBox();
            this.btoListar = new System.Windows.Forms.Button();
            this.btoEditar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReserva)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(556, 54);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(104, 97);
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // dataGridViewReserva
            // 
            this.dataGridViewReserva.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReserva.Location = new System.Drawing.Point(40, 448);
            this.dataGridViewReserva.Name = "dataGridViewReserva";
            this.dataGridViewReserva.RowTemplate.Height = 24;
            this.dataGridViewReserva.Size = new System.Drawing.Size(910, 205);
            this.dataGridViewReserva.TabIndex = 39;
            // 
            // btoCrear
            // 
            this.btoCrear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btoCrear.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btoCrear.FlatAppearance.BorderSize = 0;
            this.btoCrear.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoCrear.Location = new System.Drawing.Point(40, 669);
            this.btoCrear.Name = "btoCrear";
            this.btoCrear.Size = new System.Drawing.Size(142, 43);
            this.btoCrear.TabIndex = 38;
            this.btoCrear.Text = "Crear";
            this.btoCrear.UseVisualStyleBackColor = false;
            this.btoCrear.Click += new System.EventHandler(this.btoCrear_Click);
            // 
            // txtCodigoReserva
            // 
            this.txtCodigoReserva.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoReserva.Location = new System.Drawing.Point(40, 151);
            this.txtCodigoReserva.Name = "txtCodigoReserva";
            this.txtCodigoReserva.Size = new System.Drawing.Size(270, 30);
            this.txtCodigoReserva.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 22);
            this.label3.TabIndex = 34;
            this.label3.Text = "Código reserva:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(235, 35);
            this.label2.TabIndex = 33;
            this.label2.Text = "Reserva Salas ";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.reservaToolStripMenuItem,
            this.salasToolStripMenuItem,
            this.personaToolStripMenuItem,
            this.consultaToolStripMenuItem,
            this.equipoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(987, 28);
            this.menuStrip1.TabIndex = 44;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(57, 24);
            this.toolStripMenuItem1.Text = "Inicio";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(113, 26);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // reservaToolStripMenuItem
            // 
            this.reservaToolStripMenuItem.Name = "reservaToolStripMenuItem";
            this.reservaToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.reservaToolStripMenuItem.Text = "Reserva";
            // 
            // salasToolStripMenuItem
            // 
            this.salasToolStripMenuItem.Name = "salasToolStripMenuItem";
            this.salasToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.salasToolStripMenuItem.Text = "Salas";
            // 
            // personaToolStripMenuItem
            // 
            this.personaToolStripMenuItem.Name = "personaToolStripMenuItem";
            this.personaToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.personaToolStripMenuItem.Text = "Persona";
            // 
            // consultaToolStripMenuItem
            // 
            this.consultaToolStripMenuItem.Name = "consultaToolStripMenuItem";
            this.consultaToolStripMenuItem.Size = new System.Drawing.Size(78, 24);
            this.consultaToolStripMenuItem.Text = "Consulta";
            // 
            // equipoToolStripMenuItem
            // 
            this.equipoToolStripMenuItem.Name = "equipoToolStripMenuItem";
            this.equipoToolStripMenuItem.Size = new System.Drawing.Size(68, 24);
            this.equipoToolStripMenuItem.Text = "Equipo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 197);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 22);
            this.label1.TabIndex = 45;
            this.label1.Text = "Fecha:";
            // 
            // txtHrInicio
            // 
            this.txtHrInicio.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHrInicio.Location = new System.Drawing.Point(34, 311);
            this.txtHrInicio.Name = "txtHrInicio";
            this.txtHrInicio.Size = new System.Drawing.Size(42, 30);
            this.txtHrInicio.TabIndex = 48;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 277);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 22);
            this.label4.TabIndex = 47;
            this.label4.Text = "Hora inicio:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(378, 197);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 22);
            this.label6.TabIndex = 51;
            this.label6.Text = "Nombre sala:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(378, 275);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 22);
            this.label8.TabIndex = 55;
            this.label8.Text = "Código equipo:";
            // 
            // dateTimePickerFecha
            // 
            this.dateTimePickerFecha.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerFecha.Location = new System.Drawing.Point(40, 228);
            this.dateTimePickerFecha.Name = "dateTimePickerFecha";
            this.dateTimePickerFecha.Size = new System.Drawing.Size(270, 28);
            this.dateTimePickerFecha.TabIndex = 57;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(193, 277);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 22);
            this.label5.TabIndex = 58;
            this.label5.Text = "Hora termino:";
            // 
            // txtComentarios
            // 
            this.txtComentarios.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComentarios.Location = new System.Drawing.Point(40, 388);
            this.txtComentarios.Name = "txtComentarios";
            this.txtComentarios.Size = new System.Drawing.Size(603, 30);
            this.txtComentarios.TabIndex = 61;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(36, 357);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 22);
            this.label7.TabIndex = 60;
            this.label7.Text = "Comentarios:";
            // 
            // btoRenovar
            // 
            this.btoRenovar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btoRenovar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btoRenovar.FlatAppearance.BorderSize = 0;
            this.btoRenovar.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoRenovar.Location = new System.Drawing.Point(654, 669);
            this.btoRenovar.Name = "btoRenovar";
            this.btoRenovar.Size = new System.Drawing.Size(142, 43);
            this.btoRenovar.TabIndex = 62;
            this.btoRenovar.Text = "Renovar";
            this.btoRenovar.UseVisualStyleBackColor = false;
            this.btoRenovar.Click += new System.EventHandler(this.btoRenovar_Click);
            // 
            // btoAnular
            // 
            this.btoAnular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btoAnular.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btoAnular.FlatAppearance.BorderSize = 0;
            this.btoAnular.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoAnular.Location = new System.Drawing.Point(808, 669);
            this.btoAnular.Name = "btoAnular";
            this.btoAnular.Size = new System.Drawing.Size(142, 43);
            this.btoAnular.TabIndex = 63;
            this.btoAnular.Text = "Anular";
            this.btoAnular.UseVisualStyleBackColor = false;
            this.btoAnular.Click += new System.EventHandler(this.btoAnular_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(79, 311);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 22);
            this.label9.TabIndex = 64;
            this.label9.Text = ":";
            // 
            // txtMinInicio
            // 
            this.txtMinInicio.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMinInicio.Location = new System.Drawing.Point(101, 311);
            this.txtMinInicio.Name = "txtMinInicio";
            this.txtMinInicio.Size = new System.Drawing.Size(42, 30);
            this.txtMinInicio.TabIndex = 65;
            // 
            // txtMinTermino
            // 
            this.txtMinTermino.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMinTermino.Location = new System.Drawing.Point(260, 311);
            this.txtMinTermino.Name = "txtMinTermino";
            this.txtMinTermino.Size = new System.Drawing.Size(42, 30);
            this.txtMinTermino.TabIndex = 68;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(238, 311);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 22);
            this.label10.TabIndex = 67;
            this.label10.Text = ":";
            // 
            // txtHrTermino
            // 
            this.txtHrTermino.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHrTermino.Location = new System.Drawing.Point(193, 311);
            this.txtHrTermino.Name = "txtHrTermino";
            this.txtHrTermino.Size = new System.Drawing.Size(42, 30);
            this.txtHrTermino.TabIndex = 66;
            // 
            // cbxCodigoSala
            // 
            this.cbxCodigoSala.FormattingEnabled = true;
            this.cbxCodigoSala.Location = new System.Drawing.Point(382, 228);
            this.cbxCodigoSala.Name = "cbxCodigoSala";
            this.cbxCodigoSala.Size = new System.Drawing.Size(315, 24);
            this.cbxCodigoSala.TabIndex = 69;
            // 
            // cbxCodigoEquipo
            // 
            this.cbxCodigoEquipo.FormattingEnabled = true;
            this.cbxCodigoEquipo.Location = new System.Drawing.Point(382, 311);
            this.cbxCodigoEquipo.Name = "cbxCodigoEquipo";
            this.cbxCodigoEquipo.Size = new System.Drawing.Size(315, 24);
            this.cbxCodigoEquipo.TabIndex = 70;
            // 
            // btoListar
            // 
            this.btoListar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btoListar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btoListar.FlatAppearance.BorderSize = 0;
            this.btoListar.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoListar.Location = new System.Drawing.Point(501, 669);
            this.btoListar.Name = "btoListar";
            this.btoListar.Size = new System.Drawing.Size(142, 43);
            this.btoListar.TabIndex = 71;
            this.btoListar.Text = "Listar";
            this.btoListar.UseVisualStyleBackColor = false;
            this.btoListar.Click += new System.EventHandler(this.btoListar_Click);
            // 
            // btoEditar
            // 
            this.btoEditar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btoEditar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btoEditar.FlatAppearance.BorderSize = 0;
            this.btoEditar.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoEditar.Location = new System.Drawing.Point(347, 669);
            this.btoEditar.Name = "btoEditar";
            this.btoEditar.Size = new System.Drawing.Size(142, 43);
            this.btoEditar.TabIndex = 72;
            this.btoEditar.Text = "Buscar";
            this.btoEditar.UseVisualStyleBackColor = false;
            this.btoEditar.Click += new System.EventHandler(this.btoEditar_Click);
            // 
            // PantallaTransaccionReservaSala
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(987, 806);
            this.Controls.Add(this.btoEditar);
            this.Controls.Add(this.btoListar);
            this.Controls.Add(this.cbxCodigoEquipo);
            this.Controls.Add(this.cbxCodigoSala);
            this.Controls.Add(this.txtMinTermino);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtHrTermino);
            this.Controls.Add(this.txtMinInicio);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btoAnular);
            this.Controls.Add(this.btoRenovar);
            this.Controls.Add(this.txtComentarios);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dateTimePickerFecha);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtHrInicio);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.dataGridViewReserva);
            this.Controls.Add(this.btoCrear);
            this.Controls.Add(this.txtCodigoReserva);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "PantallaTransaccionReservaSala";
            this.Text = "Salas Vivas - Reserva";
            this.Load += new System.EventHandler(this.PantallaTransaccionReservaSala_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReserva)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dataGridViewReserva;
        private System.Windows.Forms.Button btoCrear;
        private System.Windows.Forms.TextBox txtCodigoReserva;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reservaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem personaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equipoToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtHrInicio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dateTimePickerFecha;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtComentarios;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.Button btoRenovar;
        private System.Windows.Forms.Button btoAnular;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMinInicio;
        private System.Windows.Forms.TextBox txtMinTermino;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtHrTermino;
        private System.Windows.Forms.ComboBox cbxCodigoSala;
        private System.Windows.Forms.ComboBox cbxCodigoEquipo;
        private System.Windows.Forms.Button btoListar;
        private System.Windows.Forms.Button btoEditar;
    }
}