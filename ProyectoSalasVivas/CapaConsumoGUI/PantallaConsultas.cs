﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaNegocio;
using CapaDTO;
using System.Windows.Forms;

namespace CapaConsumoGUI
{
    public partial class PantallaConsultas : Form
    {
        public PantallaConsultas()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void reservaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionReservaSala pReserva = new PantallaTransaccionReservaSala();
            pReserva.ShowDialog();
        }

        private void salasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorSala pSala = new PantallaMantenedorSala();
            pSala.ShowDialog();
        }

        private void personaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorPersona pPersona = new PantallaMantenedorPersona();
            pPersona.ShowDialog();
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaConsultas pConsultas = new PantallaConsultas();
            pConsultas.ShowDialog();
        }

        private void equipoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionEquipo pEquipo = new PantallaTransaccionEquipo();
            pEquipo.ShowDialog();
        }

        private void PantallaConsultas_Load(object sender, EventArgs e)
        {
            var data = new Dictionary<int, string>{
               {1, "Reservas de hoy"},
               {2, "Reservas este mes"},
               {3, "Cantidad de Reservas anuladas hoy"},
               {4, "Cifra actual de equipos inscritos"}
            };
            
            this.cbxTipoConsulta.DataSource = data.ToArray();
            this.cbxTipoConsulta.DisplayMember = "Value";
            this.cbxTipoConsulta.ValueMember = "Key";
        }

        private void limpiar()
        {
            this.dataGridViewConsulta.DataSource = null;
        }

        private void btoMostrar_Click(object sender, EventArgs e)
        {
            String auxConsulta = this.cbxTipoConsulta.Text;
            int mes = Convert.ToInt32(DateTime.Now.Month.ToString());
            int dia = Convert.ToInt32(DateTime.Now.Day.ToString());
            int anno = Convert.ToInt32(DateTime.Now.Year.ToString());

            if (auxConsulta == "Reservas de hoy")
            {
                if (this.btoMostrar.Text.Equals("Limpiar"))
                {
                    limpiar();
                    this.btoMostrar.Text = "Mostrar";
                }
                else
                {
                    ServiceConsulta.WebServiceConsultaSoapClient auxNegocio = new ServiceConsulta.WebServiceConsultaSoapClient();
                    this.dataGridViewConsulta.DataSource = auxNegocio.MostrarReservasHoyService(dia,mes,anno);
                    this.dataGridViewConsulta.DataMember = "ReservaSala";
                    this.btoMostrar.Text = "Limpiar";
                }
                
            }
            if (auxConsulta == "Reservas este mes")
            {
                if (this.btoMostrar.Text.Equals("Limpiar"))
                {
                    limpiar();
                    this.btoMostrar.Text = "Mostrar";
                }
                else
                {
                    ServiceConsulta.WebServiceConsultaSoapClient auxNegocio = new ServiceConsulta.WebServiceConsultaSoapClient();
                    this.dataGridViewConsulta.DataSource = auxNegocio.MostrarReservasMensualesService(mes, anno);
                    this.dataGridViewConsulta.DataMember = "ReservaSala";
                    this.btoMostrar.Text = "Limpiar";
                }
            }
            if (auxConsulta == "Cantidad de Reservas anuladas hoy")
            {
                if (this.btoMostrar.Text.Equals("Limpiar"))
                {
                    limpiar();
                    this.btoMostrar.Text = "Mostrar";
                }
                else
                {
                    ServiceConsulta.WebServiceConsultaSoapClient auxNegocio = new ServiceConsulta.WebServiceConsultaSoapClient();
                    this.dataGridViewConsulta.DataSource = auxNegocio.MostrarReservasAnuladasService(dia, mes, anno);
                    this.dataGridViewConsulta.DataMember = "ReservaSala";
                    this.btoMostrar.Text = "Limpiar";
                }
            }
            if (auxConsulta == "Cifra actual de equipos inscritos")
            {
                if (this.btoMostrar.Text.Equals("Limpiar"))
                {
                    limpiar();
                    this.btoMostrar.Text = "Mostrar";
                }
                else
                {
                    ServiceConsulta.WebServiceConsultaSoapClient auxNegocio = new ServiceConsulta.WebServiceConsultaSoapClient();
                    this.dataGridViewConsulta.DataSource = auxNegocio.MostrarCantidadEquiposService();
                    this.dataGridViewConsulta.DataMember = "equipo";
                    this.btoMostrar.Text = "Limpiar";
                }
            }
            
        }
    }
}
