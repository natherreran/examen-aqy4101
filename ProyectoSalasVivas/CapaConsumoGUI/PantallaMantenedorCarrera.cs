﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDTO;
using CapaNegocio;

namespace CapaConsumoGUI
{
    public partial class PantallaMantenedorCarrera : Form
    {
        public PantallaMantenedorCarrera()
        {
            InitializeComponent();
        }

        private void limpiar()
        {
            this.txtIdCarrera.Text = String.Empty;
            this.txtNombreCarrera.Text = String.Empty;
            this.txtIdCarrera.Focus();
            this.dataGridViewCarrera.DataSource = null;
        }


        private void habilitar()
        {
            this.txtIdCarrera.Enabled = true;
            this.txtNombreCarrera.Enabled = true;
        }

        private void desHabilitar()
        {
            this.txtIdCarrera.Enabled = false;
            this.txtNombreCarrera.Enabled = false;
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtIdCarrera.Text == "" && this.txtNombreCarrera.Text == "")
                {
                    MessageBox.Show("Complete los datos solicitados");
                }
                else
                {
                    if (Convert.ToInt32(this.txtIdCarrera.Text) <= 0)
                    {
                        MessageBox.Show("No se permite el ingreso de numero negativos");
                    }
                    else
                    {
                        ServicesCarrera.WebServiceCarreraSoapClient auxNegocioCarrera = new ServicesCarrera.WebServiceCarreraSoapClient();
                        ServicesCarrera.Carrera auxCarrera = new ServicesCarrera.Carrera();
                   

                        if (auxNegocioCarrera.buscarCarreraService(Convert.ToInt32(this.txtIdCarrera.Text)).Id_carrera.Equals(0))
                        {
                            auxCarrera.Id_carrera = Convert.ToInt32(this.txtIdCarrera.Text);
                            auxCarrera.Nombre_carrera = this.txtNombreCarrera.Text;
                            auxNegocioCarrera.grabarCarreraService(auxCarrera);
                            MessageBox.Show("Datos Guardados", "Sistema");
                            this.limpiar();
                        }
                        else
                        {
                            MessageBox.Show("Carrera ya existe", "sistema");

                        }

                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtIdCarrera.Text == "")
                {
                    MessageBox.Show("Ingrese un Id");
                }
                else
                {
                    ServicesCarrera.WebServiceCarreraSoapClient auxNegocioCarrera = new ServicesCarrera.WebServiceCarreraSoapClient();
                    

                    if (auxNegocioCarrera.buscarCarreraService(Convert.ToInt32(this.txtIdCarrera.Text)).Id_carrera.Equals(0))
                    {
                        MessageBox.Show("El Id no existe");
                    }
                    else
                    {
                        if (MessageBox.Show("Estas Seguro de Eliminar ", "sistema", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {

                            auxNegocioCarrera.eliminarCarreraService(Convert.ToInt32(this.txtIdCarrera.Text));
                            MessageBox.Show("Carrera eliminada exitosamente", "Sistema");
                            this.btnListar.Text = "Listar";
                            this.limpiar();
                            txtIdCarrera.Focus();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtIdCarrera.Text == "")
                {
                    MessageBox.Show("Ingrese un ID");
                }
                else
                {
                    if (this.btnEditar.Text.Equals("Buscar"))
                    {
                        ServicesCarrera.WebServiceCarreraSoapClient auxNegocioCarrera = new ServicesCarrera.WebServiceCarreraSoapClient();
                        ServicesCarrera.Carrera auxCarrera = new ServicesCarrera.Carrera();

                        if (auxNegocioCarrera.buscarCarreraService(Convert.ToInt32(this.txtIdCarrera.Text)).Id_carrera.Equals(0))
                        {
                            MessageBox.Show("El Id ingresado no existe");
                        }
                        else
                        {
                            auxCarrera = auxNegocioCarrera.buscarCarreraService(Convert.ToInt32(this.txtIdCarrera.Text));

                            this.txtIdCarrera.Text = auxCarrera.Id_carrera.ToString();
                            this.txtNombreCarrera.Text = auxCarrera.Nombre_carrera;
                            this.btnEditar.Text = "Editar";
                        }

                    }
                    else
                    {
                        if (this.btnEditar.Text.Equals("Editar"))
                        {
                            if (this.txtNombreCarrera.Text == "")
                            {
                                MessageBox.Show("Ingrese un nombre");
                            }
                            else
                            {
                                if (MessageBox.Show("Estas Seguro de actualizar ", "sistema", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    ServicesCarrera.WebServiceCarreraSoapClient auxNegocioCarrera = new ServicesCarrera.WebServiceCarreraSoapClient();
                                    ServicesCarrera.Carrera auxCarrera = new ServicesCarrera.Carrera();

                                    auxCarrera.Id_carrera = Convert.ToInt32(this.txtIdCarrera.Text);
                                    auxCarrera.Nombre_carrera = this.txtNombreCarrera.Text;
                                    auxNegocioCarrera.actualizarCarreraService(auxCarrera);
                                    MessageBox.Show("Datos actualizados exitosamente", "Sistema");
                                    this.btnEditar.Text = "Buscar";
                                    this.btnListar.Text = "Listar";
                                    this.limpiar();
                                    txtIdCarrera.Focus();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
            
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            if (this.btnListar.Text.Equals("Limpiar"))
            {
                limpiar();
                this.btnListar.Text = "Listar";
            }
            else
            {
                ServicesCarrera.WebServiceCarreraSoapClient auxNegocioCarrera = new ServicesCarrera.WebServiceCarreraSoapClient();
                ServicesCarrera.Carrera auxCarrera = new ServicesCarrera.Carrera();
                this.dataGridViewCarrera.DataSource = auxNegocioCarrera.listarCarreraService();
                this.dataGridViewCarrera.DataMember = "Carrera";
                this.btnListar.Text = "Limpiar";

            }
                
        }
        
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void reservaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionReservaSala pReserva = new PantallaTransaccionReservaSala();
            pReserva.ShowDialog();
        }

        private void salasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorSala pSala = new PantallaMantenedorSala();
            pSala.ShowDialog();
        }

        private void personaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorPersona pPersona = new PantallaMantenedorPersona();
            pPersona.ShowDialog();
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaConsultas pConsultas = new PantallaConsultas();
            pConsultas.ShowDialog();
        }

        private void equipoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionEquipo pEquipo = new PantallaTransaccionEquipo();
            pEquipo.ShowDialog();
        }

        private void PantallaMantenedorCarrera_Load(object sender, EventArgs e)
        {
            
        }
    }
}
