﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaConsumoGUI
{
    public partial class PantallaTransaccionEquipo : Form
    {
        public PantallaTransaccionEquipo()
        {
            InitializeComponent();
        }

        public void limpiar()
        {
            this.txtCantidadIntegrantes.Clear();
            this.txtCodigoEquipo.Clear();
            this.txtRutEncargado.Clear();
            this.rbtnDocente.Checked = false;
            this.rbtnEstudiante.Checked = false;
            this.dataGridViewEquipo.DataSource = null;
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void reservaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionReservaSala pReserva = new PantallaTransaccionReservaSala();
            pReserva.ShowDialog();
        }

        private void salasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorSala pSala = new PantallaMantenedorSala();
            pSala.ShowDialog();
        }

        private void personaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorPersona pPersona = new PantallaMantenedorPersona();
            pPersona.ShowDialog();
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaConsultas pConsultas = new PantallaConsultas();
            pConsultas.ShowDialog();
        }

        private void equipoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionEquipo pEquipo = new PantallaTransaccionEquipo();
            pEquipo.ShowDialog();
        }

        private void btoCrear_Click(object sender, EventArgs e)
        {
            ServiceEquipo.Equipo auxEquipo = new ServiceEquipo.Equipo();
            this.btoListar.Text = "Listar";
            auxEquipo.Cod_equipo = Convert.ToInt32(this.txtCodigoEquipo.Text);
            auxEquipo.Cantidad = Convert.ToInt32(this.txtCantidadIntegrantes.Text);
            auxEquipo.Rut_persona = this.txtRutEncargado.Text;
            if (this.rbtnDocente.Checked)
            {
                auxEquipo.Es_docente = 1;
            }
            else
            {
                auxEquipo.Es_docente = 0;
            }
            if (this.rbtnEstudiante.Checked)
            {
                auxEquipo.Es_encargado = 1;
            }
            else
            {
                auxEquipo.Es_encargado = 0;
            }
            ServiceEquipo.WebServiceEquipoSoapClient auxNegocio = new ServiceEquipo.WebServiceEquipoSoapClient();
            auxNegocio.grabarEquipoService(auxEquipo);
            MessageBox.Show("El equipo a sido grabado exitosamente");
            limpiar();
        }

        private void btoEliminar_Click(object sender, EventArgs e)
        {
            this.btoListar.Text = "Listar";
            int auxId = Convert.ToInt32(this.txtCodigoEquipo.Text);
            ServiceEquipo.WebServiceEquipoSoapClient auxNegocio = new ServiceEquipo.WebServiceEquipoSoapClient();
            auxNegocio.eliminarEquipoService(auxId);
            MessageBox.Show("El equipo a sido eliminado exitosamente");
            limpiar();
        }

        private void btoEditar_Click(object sender, EventArgs e)
        {
            this.btoListar.Text = "Listar";
            ServiceEquipo.Equipo auxEquipo = new ServiceEquipo.Equipo();
            auxEquipo.Cod_equipo = Convert.ToInt32(this.txtCodigoEquipo.Text);
            auxEquipo.Cantidad = Convert.ToInt32(this.txtCantidadIntegrantes.Text);
            auxEquipo.Rut_persona = this.txtRutEncargado.Text;
            if (this.rbtnDocente.Checked)
            {
                auxEquipo.Es_docente = 1;
            }
            else
            {
                auxEquipo.Es_docente = 0;
            }
            if (this.rbtnEstudiante.Checked)
            {
                auxEquipo.Es_encargado = 1;
            }
            else
            {
                auxEquipo.Es_encargado = 0;
            }
            ServiceEquipo.WebServiceEquipoSoapClient auxNegocio = new ServiceEquipo.WebServiceEquipoSoapClient();
            auxNegocio.actualizarEquipoService(auxEquipo);
            MessageBox.Show("El equipo a sido actualizado exitosamente");
            limpiar();
        }

        private void btoListar_Click(object sender, EventArgs e)
        {
            if (this.btoListar.Text.Equals("Limpiar"))
            {
                limpiar();
                this.btoListar.Text = "Listar";
            }
            else
            {
                ServiceEquipo.WebServiceEquipoSoapClient auxNegocio = new ServiceEquipo.WebServiceEquipoSoapClient();
                this.dataGridViewEquipo.DataSource = auxNegocio.listarEquipoService();
                this.dataGridViewEquipo.DataMember = "Equipo";
                this.btoListar.Text = "Limpiar";
            }
        }
    }
}
