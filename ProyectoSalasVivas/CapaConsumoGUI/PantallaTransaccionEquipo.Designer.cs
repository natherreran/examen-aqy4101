﻿namespace CapaConsumoGUI
{
    partial class PantallaTransaccionEquipo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PantallaTransaccionEquipo));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btoListar = new System.Windows.Forms.Button();
            this.btoEditar = new System.Windows.Forms.Button();
            this.btoEliminar = new System.Windows.Forms.Button();
            this.dataGridViewEquipo = new System.Windows.Forms.DataGridView();
            this.btoCrear = new System.Windows.Forms.Button();
            this.txtCodigoEquipo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reservaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCantidadIntegrantes = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.rbtnDocente = new System.Windows.Forms.RadioButton();
            this.rbtnEstudiante = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtRutEncargado = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEquipo)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(537, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(101, 97);
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // btoListar
            // 
            this.btoListar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btoListar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btoListar.FlatAppearance.BorderSize = 0;
            this.btoListar.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoListar.Location = new System.Drawing.Point(504, 583);
            this.btoListar.Name = "btoListar";
            this.btoListar.Size = new System.Drawing.Size(139, 43);
            this.btoListar.TabIndex = 42;
            this.btoListar.Text = "Listar";
            this.btoListar.UseVisualStyleBackColor = false;
            this.btoListar.Click += new System.EventHandler(this.btoListar_Click);
            // 
            // btoEditar
            // 
            this.btoEditar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btoEditar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btoEditar.FlatAppearance.BorderSize = 0;
            this.btoEditar.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoEditar.Location = new System.Drawing.Point(350, 583);
            this.btoEditar.Name = "btoEditar";
            this.btoEditar.Size = new System.Drawing.Size(139, 43);
            this.btoEditar.TabIndex = 41;
            this.btoEditar.Text = "Editar";
            this.btoEditar.UseVisualStyleBackColor = false;
            this.btoEditar.Click += new System.EventHandler(this.btoEditar_Click);
            // 
            // btoEliminar
            // 
            this.btoEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btoEliminar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btoEliminar.FlatAppearance.BorderSize = 0;
            this.btoEliminar.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoEliminar.Location = new System.Drawing.Point(196, 583);
            this.btoEliminar.Name = "btoEliminar";
            this.btoEliminar.Size = new System.Drawing.Size(139, 43);
            this.btoEliminar.TabIndex = 40;
            this.btoEliminar.Text = "Eliminar";
            this.btoEliminar.UseVisualStyleBackColor = false;
            this.btoEliminar.Click += new System.EventHandler(this.btoEliminar_Click);
            // 
            // dataGridViewEquipo
            // 
            this.dataGridViewEquipo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEquipo.Location = new System.Drawing.Point(43, 348);
            this.dataGridViewEquipo.Name = "dataGridViewEquipo";
            this.dataGridViewEquipo.RowTemplate.Height = 24;
            this.dataGridViewEquipo.Size = new System.Drawing.Size(600, 205);
            this.dataGridViewEquipo.TabIndex = 39;
            // 
            // btoCrear
            // 
            this.btoCrear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btoCrear.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btoCrear.FlatAppearance.BorderSize = 0;
            this.btoCrear.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btoCrear.Location = new System.Drawing.Point(43, 583);
            this.btoCrear.Name = "btoCrear";
            this.btoCrear.Size = new System.Drawing.Size(139, 43);
            this.btoCrear.TabIndex = 38;
            this.btoCrear.Text = "Crear";
            this.btoCrear.UseVisualStyleBackColor = false;
            this.btoCrear.Click += new System.EventHandler(this.btoCrear_Click);
            // 
            // txtCodigoEquipo
            // 
            this.txtCodigoEquipo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoEquipo.Location = new System.Drawing.Point(43, 169);
            this.txtCodigoEquipo.Name = "txtCodigoEquipo";
            this.txtCodigoEquipo.Size = new System.Drawing.Size(257, 30);
            this.txtCodigoEquipo.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(39, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 22);
            this.label3.TabIndex = 34;
            this.label3.Text = "Código equipo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 35);
            this.label2.TabIndex = 33;
            this.label2.Text = "Equipo";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.reservaToolStripMenuItem,
            this.salasToolStripMenuItem,
            this.personaToolStripMenuItem,
            this.consultaToolStripMenuItem,
            this.equipoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(683, 28);
            this.menuStrip1.TabIndex = 45;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(57, 24);
            this.toolStripMenuItem1.Text = "Inicio";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(113, 26);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // reservaToolStripMenuItem
            // 
            this.reservaToolStripMenuItem.Name = "reservaToolStripMenuItem";
            this.reservaToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.reservaToolStripMenuItem.Text = "Reserva";
            // 
            // salasToolStripMenuItem
            // 
            this.salasToolStripMenuItem.Name = "salasToolStripMenuItem";
            this.salasToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.salasToolStripMenuItem.Text = "Salas";
            // 
            // personaToolStripMenuItem
            // 
            this.personaToolStripMenuItem.Name = "personaToolStripMenuItem";
            this.personaToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.personaToolStripMenuItem.Text = "Persona";
            // 
            // consultaToolStripMenuItem
            // 
            this.consultaToolStripMenuItem.Name = "consultaToolStripMenuItem";
            this.consultaToolStripMenuItem.Size = new System.Drawing.Size(78, 24);
            this.consultaToolStripMenuItem.Text = "Consulta";
            // 
            // equipoToolStripMenuItem
            // 
            this.equipoToolStripMenuItem.Name = "equipoToolStripMenuItem";
            this.equipoToolStripMenuItem.Size = new System.Drawing.Size(68, 24);
            this.equipoToolStripMenuItem.Text = "Equipo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 245);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 22);
            this.label1.TabIndex = 46;
            this.label1.Text = "Rut Encargado:";
            // 
            // txtCantidadIntegrantes
            // 
            this.txtCantidadIntegrantes.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCantidadIntegrantes.Location = new System.Drawing.Point(571, 171);
            this.txtCantidadIntegrantes.Name = "txtCantidadIntegrantes";
            this.txtCantidadIntegrantes.Size = new System.Drawing.Size(47, 30);
            this.txtCantidadIntegrantes.TabIndex = 49;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(388, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(177, 22);
            this.label5.TabIndex = 48;
            this.label5.Text = "Cantidad integrantes:";
            // 
            // rbtnDocente
            // 
            this.rbtnDocente.AutoSize = true;
            this.rbtnDocente.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnDocente.Location = new System.Drawing.Point(19, 35);
            this.rbtnDocente.Name = "rbtnDocente";
            this.rbtnDocente.Size = new System.Drawing.Size(93, 25);
            this.rbtnDocente.TabIndex = 52;
            this.rbtnDocente.TabStop = true;
            this.rbtnDocente.Text = "Docente";
            this.rbtnDocente.UseVisualStyleBackColor = true;
            // 
            // rbtnEstudiante
            // 
            this.rbtnEstudiante.AutoSize = true;
            this.rbtnEstudiante.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnEstudiante.Location = new System.Drawing.Point(137, 35);
            this.rbtnEstudiante.Name = "rbtnEstudiante";
            this.rbtnEstudiante.Size = new System.Drawing.Size(108, 25);
            this.rbtnEstudiante.TabIndex = 53;
            this.rbtnEstudiante.TabStop = true;
            this.rbtnEstudiante.Text = "Estudiante";
            this.rbtnEstudiante.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtnDocente);
            this.groupBox1.Controls.Add(this.rbtnEstudiante);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(392, 245);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(251, 76);
            this.groupBox1.TabIndex = 55;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo Encargado";
            // 
            // txtRutEncargado
            // 
            this.txtRutEncargado.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRutEncargado.Location = new System.Drawing.Point(43, 275);
            this.txtRutEncargado.Name = "txtRutEncargado";
            this.txtRutEncargado.Size = new System.Drawing.Size(257, 30);
            this.txtRutEncargado.TabIndex = 56;
            // 
            // PantallaTransaccionEquipo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(683, 686);
            this.Controls.Add(this.txtRutEncargado);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtCantidadIntegrantes);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btoListar);
            this.Controls.Add(this.btoEditar);
            this.Controls.Add(this.btoEliminar);
            this.Controls.Add(this.dataGridViewEquipo);
            this.Controls.Add(this.btoCrear);
            this.Controls.Add(this.txtCodigoEquipo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "PantallaTransaccionEquipo";
            this.Text = "Salas vivas - Equipo";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEquipo)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btoListar;
        private System.Windows.Forms.Button btoEditar;
        private System.Windows.Forms.Button btoEliminar;
        private System.Windows.Forms.DataGridView dataGridViewEquipo;
        private System.Windows.Forms.Button btoCrear;
        private System.Windows.Forms.TextBox txtCodigoEquipo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reservaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem personaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equipoToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCantidadIntegrantes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbtnDocente;
        private System.Windows.Forms.RadioButton rbtnEstudiante;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtRutEncargado;
    }
}