﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaConsumoGUI
{
    public partial class PantallaTransaccionReservaSala : Form
    {
        public PantallaTransaccionReservaSala()
        {
            InitializeComponent();
        }

        public void limpiar()
        {
            this.txtHrInicio.Clear();
            this.txtHrTermino.Clear();
            this.txtMinInicio.Clear();
            this.txtMinTermino.Clear();
            this.txtComentarios.Clear();
            this.txtCodigoReserva.Clear();
            this.cbxCodigoEquipo.Text = String.Empty;
            this.cbxCodigoSala.Text = String.Empty;
            this.dateTimePickerFecha.Text = null;
            this.dataGridViewReserva.DataSource = null;
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void reservaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionReservaSala pReserva = new PantallaTransaccionReservaSala();
            pReserva.ShowDialog();
        }

        private void salasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorSala pSala = new PantallaMantenedorSala();
            pSala.ShowDialog();
        }

        private void personaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorPersona pPersona = new PantallaMantenedorPersona();
            pPersona.ShowDialog();
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaConsultas pConsultas = new PantallaConsultas();
            pConsultas.ShowDialog();
        }

        private void equipoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionEquipo pEquipo = new PantallaTransaccionEquipo();
            pEquipo.ShowDialog();
        }

        private void btoCrear_Click(object sender, EventArgs e)
        {
            ServiceReserva.WebServiceReservaSoapClient auxNegocio = new ServiceReserva.WebServiceReservaSoapClient();
            ServiceReserva.ReservarSala auxReserva = new ServiceReserva.ReservarSala();
            auxReserva.Codigo_reserva = Convert.ToInt32(this.txtCodigoReserva.Text);
            auxReserva.Hora_inicio_reserva = (this.txtHrInicio.Text + ":" + this.txtMinInicio.Text);
            auxReserva.Hora_termino_reserva = (this.txtHrTermino.Text + ":" + this.txtMinTermino.Text);
            auxReserva.Observacion_reserva = this.txtComentarios.Text;
            auxReserva.Fecha_reserva = this.dateTimePickerFecha.Value;
            auxReserva.Id_sala = Convert.ToInt32(this.cbxCodigoSala.SelectedValue);
            auxReserva.Cod_equipo = Convert.ToInt32(this.cbxCodigoEquipo.SelectedValue);

    
            auxNegocio.grabarReservaSalaService(auxReserva);
            auxNegocio.grabarReservaSalaHistoricoService(auxReserva);
            MessageBox.Show("Se ha guardado reserva exitosamente!");
            limpiar();
        }


        private void btoEditar_Click(object sender, EventArgs e)
        {
            if (this.btoEditar.Text.Equals("Buscar"))
            {

                ServiceReserva.WebServiceReservaSoapClient auxNegocio = new ServiceReserva.WebServiceReservaSoapClient();
                ServiceReserva.ReservarSala auxReserva = new ServiceReserva.ReservarSala();
                this.btoEditar.Text = "Editar";
                int auxCodigo = int.Parse(this.txtCodigoReserva.Text);
                auxReserva = auxNegocio.buscarReservarSalaService(auxCodigo);
                this.txtCodigoReserva.Text = Convert.ToString(auxReserva.Codigo_reserva);
                this.txtComentarios.Text = auxReserva.Observacion_reserva;
                this.txtHrInicio.Text = Convert.ToString(auxReserva.Hora_inicio_reserva.Length - 3);
                this.txtMinInicio.Text = Convert.ToString(auxReserva.Hora_inicio_reserva.Substring(3));
                this.txtHrTermino.Text = Convert.ToString(auxReserva.Hora_termino_reserva.Length - 3);
                this.txtMinTermino.Text = Convert.ToString(auxReserva.Hora_termino_reserva.Substring(3));
                this.cbxCodigoEquipo.Text = Convert.ToString(auxReserva.Cod_equipo);
                this.cbxCodigoSala.Text = Convert.ToString(auxReserva.Id_sala);
                
            }
            else
            {
                ServiceReserva.WebServiceReservaSoapClient auxNegocio = new ServiceReserva.WebServiceReservaSoapClient();
                ServiceReserva.ReservarSala auxReserva = new ServiceReserva.ReservarSala();
                auxReserva.Codigo_reserva = Convert.ToInt32(this.txtCodigoReserva.Text);
                auxReserva.Hora_inicio_reserva = this.txtHrInicio.Text + ':' + this.txtMinInicio.Text;
                auxReserva.Hora_termino_reserva = this.txtHrTermino.Text + ':' + this.txtMinTermino.Text;
                auxReserva.Observacion_reserva = this.txtComentarios.Text;
                auxReserva.Fecha_reserva = this.dateTimePickerFecha.Value;
                auxReserva.Id_sala = Convert.ToInt32(this.cbxCodigoSala.SelectedValue);
                auxReserva.Cod_equipo = Convert.ToInt32(this.cbxCodigoEquipo.SelectedValue);
                auxNegocio.actualizarReservarSalaService(auxReserva);
                MessageBox.Show("Se ha actualizado reserva exitosamente!");
                limpiar();
                this.btoEditar.Text = "Buscar";
            }
            
        }

        private void btoListar_Click(object sender, EventArgs e)
        {
            if (this.btoListar.Text.Equals("Limpiar"))
            {
                limpiar();
                this.btoListar.Text = "Listar";
            }
            else
            {
                ServiceReserva.WebServiceReservaSoapClient auxNegocio = new ServiceReserva.WebServiceReservaSoapClient();
                this.dataGridViewReserva.DataSource = auxNegocio.listarReservarSalaService();
                this.dataGridViewReserva.DataMember = "ReservaSala";
                this.btoListar.Text = "Limpiar";
            }
        }

        private void btoRenovar_Click(object sender, EventArgs e)
        {

        }

        private void PantallaTransaccionReservaSala_Load(object sender, EventArgs e)
        {
            ServiceReserva.WebServiceReservaSoapClient auxNegocio = new ServiceReserva.WebServiceReservaSoapClient();
            this.cbxCodigoEquipo.DisplayMember = "cod_equipo";
            this.cbxCodigoEquipo.ValueMember = "cod_equipo";
            this.cbxCodigoEquipo.DataSource = auxNegocio.MostrarCodEquipoService();

            this.cbxCodigoSala.DisplayMember = "nombre_sala"; 
            this.cbxCodigoSala.ValueMember = "id_sala";
            this.cbxCodigoSala.DataSource = auxNegocio.MostrarNombreSalaService();

            limpiar();
        }

        private void btoAnular_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas Seguro de anular la reserva ", "sistema", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ServiceReserva.WebServiceReservaSoapClient auxNegocio = new ServiceReserva.WebServiceReservaSoapClient();
                
                this.btoListar.Text = "Listar";
                int auxId = Convert.ToInt32(this.txtCodigoReserva.Text);
                auxNegocio.anularReservarSalaService(auxId);
                MessageBox.Show("La reserva a sido anulada exitosamente");
                limpiar();
            }
            limpiar();
        }
    }
}
