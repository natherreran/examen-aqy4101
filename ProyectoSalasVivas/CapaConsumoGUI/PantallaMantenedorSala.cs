﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaConsumoGUI
{
    public partial class PantallaMantenedorSala : Form
    {
        public PantallaMantenedorSala()
        {
            InitializeComponent();
        }

        public void limpiar()
        {
            this.txtCapacidad.Clear();
            this.txtCodigoSala.Clear();
            this.txtDescripcion.Clear();
            this.txtNombreSala.Clear();
            this.dataGridViewSalas.DataSource = null;
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void reservaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionReservaSala pReserva = new PantallaTransaccionReservaSala();
            pReserva.ShowDialog();
        }

        private void salasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorSala pSala = new PantallaMantenedorSala();
            pSala.ShowDialog();
        }

        private void personaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorPersona pPersona = new PantallaMantenedorPersona();
            pPersona.ShowDialog();
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaConsultas pConsultas = new PantallaConsultas();
            pConsultas.ShowDialog();
        }

        private void equipoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionEquipo pEquipo = new PantallaTransaccionEquipo();
            pEquipo.ShowDialog();
        }

        private void btoCrear_Click(object sender, EventArgs e)
        {
            ServiceSala.WebServiceSalaSoapClient auxNegocio = new ServiceSala.WebServiceSalaSoapClient();
            ServiceSala.Sala auxSala = new ServiceSala.Sala();
            
            auxSala.Id_sala = Convert.ToInt32(this.txtCodigoSala.Text);
            auxSala.Descripcion_sala = this.txtDescripcion.Text;
            auxSala.Cantidad_integrante = Convert.ToInt32(this.txtCapacidad.Text);
            auxSala.Nombre_sala = this.txtNombreSala.Text;
            
            auxNegocio.grabarSalaService(auxSala);
            MessageBox.Show("Sala guardada exitosamente", "Sistema");
            limpiar();
        }

        private void btoEliminar_Click(object sender, EventArgs e)
        {
            ServiceSala.WebServiceSalaSoapClient auxNegocio = new ServiceSala.WebServiceSalaSoapClient();
            int auxId = Convert.ToInt32(this.txtCodigoSala.Text);
            auxNegocio.eliminarSalaService(auxId);
            MessageBox.Show("Sala eliminada exitosamente", "Sistema");
            limpiar();
        }

        private void btoEditar_Click(object sender, EventArgs e)
        {
            ServiceSala.WebServiceSalaSoapClient auxNegocio = new ServiceSala.WebServiceSalaSoapClient();
            ServiceSala.Sala auxSala = new ServiceSala.Sala();
            auxSala.Id_sala = Convert.ToInt32(this.txtCodigoSala.Text);
            auxSala.Descripcion_sala = this.txtDescripcion.Text;
            auxSala.Cantidad_integrante = Convert.ToInt32(this.txtCapacidad.Text);
            auxSala.Nombre_sala = this.txtNombreSala.Text;
            auxNegocio.actualizarSalaService(auxSala);
            MessageBox.Show("Sala modificada exitosamente", "Sistema");
            limpiar();
        }

        private void btoListar_Click(object sender, EventArgs e)
        {
            if (this.btoListar.Text.Equals("Limpiar"))
            {
                limpiar();
                this.btoListar.Text = "Listar";
            }
            else
            {
                ServiceSala.WebServiceSalaSoapClient auxNegocio = new ServiceSala.WebServiceSalaSoapClient();
                ServiceSala.Sala auxSala = new ServiceSala.Sala();
                this.dataGridViewSalas.DataSource = auxNegocio.listarSalaService();
                this.dataGridViewSalas.DataMember = "Sala";
                this.btoListar.Text = "Limpiar";
            }
        }
    }
}
