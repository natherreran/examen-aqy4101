﻿namespace CapaConsumoGUI
{
    partial class PantallaMantenedorPersona
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PantallaMantenedorPersona));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reservaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnListarPersona = new System.Windows.Forms.Button();
            this.btnEditarPersona = new System.Windows.Forms.Button();
            this.btnEliminarPersona = new System.Windows.Forms.Button();
            this.dataGridViewPersona = new System.Windows.Forms.DataGridView();
            this.btnCrearPersona = new System.Windows.Forms.Button();
            this.txtNombrePersona = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRutPersona = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtApellidoPersona = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTelefonoPersona = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtEmailPersona = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbMujerPersona = new System.Windows.Forms.RadioButton();
            this.rbHombrePersona = new System.Windows.Forms.RadioButton();
            this.rbOtroPersona = new System.Windows.Forms.RadioButton();
            this.cbxCarrera = new System.Windows.Forms.ComboBox();
            this.cbxCategoria = new System.Windows.Forms.ComboBox();
            this.txtJornadaPersona = new System.Windows.Forms.TextBox();
            this.btnAgregarCarrera = new System.Windows.Forms.Button();
            this.btnAgregarCategoria = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPersona)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.reservaToolStripMenuItem,
            this.salasToolStripMenuItem,
            this.personaToolStripMenuItem,
            this.consultaToolStripMenuItem,
            this.equipoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(938, 24);
            this.menuStrip1.TabIndex = 36;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(48, 20);
            this.toolStripMenuItem1.Text = "Inicio";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // reservaToolStripMenuItem
            // 
            this.reservaToolStripMenuItem.Name = "reservaToolStripMenuItem";
            this.reservaToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reservaToolStripMenuItem.Text = "Reserva";
            // 
            // salasToolStripMenuItem
            // 
            this.salasToolStripMenuItem.Name = "salasToolStripMenuItem";
            this.salasToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.salasToolStripMenuItem.Text = "Salas";
            // 
            // personaToolStripMenuItem
            // 
            this.personaToolStripMenuItem.Name = "personaToolStripMenuItem";
            this.personaToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.personaToolStripMenuItem.Text = "Persona";
            // 
            // consultaToolStripMenuItem
            // 
            this.consultaToolStripMenuItem.Name = "consultaToolStripMenuItem";
            this.consultaToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.consultaToolStripMenuItem.Text = "Consulta";
            // 
            // equipoToolStripMenuItem
            // 
            this.equipoToolStripMenuItem.Name = "equipoToolStripMenuItem";
            this.equipoToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.equipoToolStripMenuItem.Text = "Equipo";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(627, 28);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(130, 121);
            this.pictureBox1.TabIndex = 47;
            this.pictureBox1.TabStop = false;
            // 
            // btnListarPersona
            // 
            this.btnListarPersona.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnListarPersona.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnListarPersona.FlatAppearance.BorderSize = 0;
            this.btnListarPersona.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListarPersona.Location = new System.Drawing.Point(613, 620);
            this.btnListarPersona.Margin = new System.Windows.Forms.Padding(4);
            this.btnListarPersona.Name = "btnListarPersona";
            this.btnListarPersona.Size = new System.Drawing.Size(178, 54);
            this.btnListarPersona.TabIndex = 46;
            this.btnListarPersona.Text = "Listar";
            this.btnListarPersona.UseVisualStyleBackColor = false;
            this.btnListarPersona.Click += new System.EventHandler(this.btnListarPersona_Click);
            // 
            // btnEditarPersona
            // 
            this.btnEditarPersona.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEditarPersona.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnEditarPersona.FlatAppearance.BorderSize = 0;
            this.btnEditarPersona.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarPersona.Location = new System.Drawing.Point(421, 620);
            this.btnEditarPersona.Margin = new System.Windows.Forms.Padding(4);
            this.btnEditarPersona.Name = "btnEditarPersona";
            this.btnEditarPersona.Size = new System.Drawing.Size(178, 54);
            this.btnEditarPersona.TabIndex = 45;
            this.btnEditarPersona.Text = "Buscar";
            this.btnEditarPersona.UseVisualStyleBackColor = false;
            this.btnEditarPersona.Click += new System.EventHandler(this.btnEditarPersona_Click);
            // 
            // btnEliminarPersona
            // 
            this.btnEliminarPersona.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEliminarPersona.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnEliminarPersona.FlatAppearance.BorderSize = 0;
            this.btnEliminarPersona.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarPersona.Location = new System.Drawing.Point(229, 620);
            this.btnEliminarPersona.Margin = new System.Windows.Forms.Padding(4);
            this.btnEliminarPersona.Name = "btnEliminarPersona";
            this.btnEliminarPersona.Size = new System.Drawing.Size(178, 54);
            this.btnEliminarPersona.TabIndex = 44;
            this.btnEliminarPersona.Text = "Eliminar";
            this.btnEliminarPersona.UseVisualStyleBackColor = false;
            this.btnEliminarPersona.Click += new System.EventHandler(this.btnEliminarPersona_Click);
            // 
            // dataGridViewPersona
            // 
            this.dataGridViewPersona.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPersona.Location = new System.Drawing.Point(48, 437);
            this.dataGridViewPersona.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewPersona.Name = "dataGridViewPersona";
            this.dataGridViewPersona.RowTemplate.Height = 24;
            this.dataGridViewPersona.Size = new System.Drawing.Size(727, 165);
            this.dataGridViewPersona.TabIndex = 43;
            // 
            // btnCrearPersona
            // 
            this.btnCrearPersona.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnCrearPersona.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnCrearPersona.FlatAppearance.BorderSize = 0;
            this.btnCrearPersona.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearPersona.Location = new System.Drawing.Point(37, 620);
            this.btnCrearPersona.Margin = new System.Windows.Forms.Padding(4);
            this.btnCrearPersona.Name = "btnCrearPersona";
            this.btnCrearPersona.Size = new System.Drawing.Size(178, 54);
            this.btnCrearPersona.TabIndex = 42;
            this.btnCrearPersona.Text = "Crear";
            this.btnCrearPersona.UseVisualStyleBackColor = false;
            this.btnCrearPersona.Click += new System.EventHandler(this.btnCrearPersona_Click);
            // 
            // txtNombrePersona
            // 
            this.txtNombrePersona.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombrePersona.Location = new System.Drawing.Point(48, 194);
            this.txtNombrePersona.Margin = new System.Windows.Forms.Padding(4);
            this.txtNombrePersona.Name = "txtNombrePersona";
            this.txtNombrePersona.Size = new System.Drawing.Size(320, 26);
            this.txtNombrePersona.TabIndex = 41;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(47, 170);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 19);
            this.label4.TabIndex = 40;
            this.label4.Text = "Nombre";
            // 
            // txtRutPersona
            // 
            this.txtRutPersona.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRutPersona.Location = new System.Drawing.Point(48, 134);
            this.txtRutPersona.Margin = new System.Windows.Forms.Padding(4);
            this.txtRutPersona.Name = "txtRutPersona";
            this.txtRutPersona.Size = new System.Drawing.Size(320, 26);
            this.txtRutPersona.TabIndex = 39;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(47, 112);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 19);
            this.label3.TabIndex = 38;
            this.label3.Text = "Rut:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(52, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 29);
            this.label2.TabIndex = 37;
            this.label2.Text = "Personas";
            // 
            // txtApellidoPersona
            // 
            this.txtApellidoPersona.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoPersona.Location = new System.Drawing.Point(48, 252);
            this.txtApellidoPersona.Margin = new System.Windows.Forms.Padding(4);
            this.txtApellidoPersona.Name = "txtApellidoPersona";
            this.txtApellidoPersona.Size = new System.Drawing.Size(320, 26);
            this.txtApellidoPersona.TabIndex = 49;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 226);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 19);
            this.label1.TabIndex = 48;
            this.label1.Text = "Apellidos";
            // 
            // txtTelefonoPersona
            // 
            this.txtTelefonoPersona.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefonoPersona.Location = new System.Drawing.Point(49, 386);
            this.txtTelefonoPersona.Margin = new System.Windows.Forms.Padding(4);
            this.txtTelefonoPersona.Name = "txtTelefonoPersona";
            this.txtTelefonoPersona.Size = new System.Drawing.Size(319, 26);
            this.txtTelefonoPersona.TabIndex = 53;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(47, 363);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 19);
            this.label6.TabIndex = 52;
            this.label6.Text = "Teléfono:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(447, 366);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 19);
            this.label7.TabIndex = 54;
            this.label7.Text = "Jornada:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(447, 304);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 19);
            this.label8.TabIndex = 56;
            this.label8.Text = "Categoría:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(447, 232);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 19);
            this.label9.TabIndex = 58;
            this.label9.Text = "Carrera:";
            // 
            // txtEmailPersona
            // 
            this.txtEmailPersona.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailPersona.Location = new System.Drawing.Point(451, 193);
            this.txtEmailPersona.Margin = new System.Windows.Forms.Padding(4);
            this.txtEmailPersona.Name = "txtEmailPersona";
            this.txtEmailPersona.Size = new System.Drawing.Size(315, 26);
            this.txtEmailPersona.TabIndex = 61;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(447, 170);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 19);
            this.label10.TabIndex = 60;
            this.label10.Text = "Email:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbMujerPersona);
            this.groupBox1.Controls.Add(this.rbHombrePersona);
            this.groupBox1.Controls.Add(this.rbOtroPersona);
            this.groupBox1.Location = new System.Drawing.Point(48, 293);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(320, 62);
            this.groupBox1.TabIndex = 62;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Género:";
            // 
            // rbMujerPersona
            // 
            this.rbMujerPersona.AutoSize = true;
            this.rbMujerPersona.Font = new System.Drawing.Font("Times New Roman", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbMujerPersona.Location = new System.Drawing.Point(115, 24);
            this.rbMujerPersona.Name = "rbMujerPersona";
            this.rbMujerPersona.Size = new System.Drawing.Size(52, 18);
            this.rbMujerPersona.TabIndex = 1;
            this.rbMujerPersona.TabStop = true;
            this.rbMujerPersona.Text = "Mujer";
            this.rbMujerPersona.UseVisualStyleBackColor = true;
            // 
            // rbHombrePersona
            // 
            this.rbHombrePersona.AutoSize = true;
            this.rbHombrePersona.Font = new System.Drawing.Font("Times New Roman", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbHombrePersona.Location = new System.Drawing.Point(7, 24);
            this.rbHombrePersona.Name = "rbHombrePersona";
            this.rbHombrePersona.Size = new System.Drawing.Size(62, 18);
            this.rbHombrePersona.TabIndex = 0;
            this.rbHombrePersona.TabStop = true;
            this.rbHombrePersona.Text = "Hombre";
            this.rbHombrePersona.UseVisualStyleBackColor = true;
            // 
            // rbOtroPersona
            // 
            this.rbOtroPersona.AutoSize = true;
            this.rbOtroPersona.Font = new System.Drawing.Font("Times New Roman", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbOtroPersona.Location = new System.Drawing.Point(192, 24);
            this.rbOtroPersona.Name = "rbOtroPersona";
            this.rbOtroPersona.Size = new System.Drawing.Size(47, 18);
            this.rbOtroPersona.TabIndex = 2;
            this.rbOtroPersona.TabStop = true;
            this.rbOtroPersona.Text = "Otro";
            this.rbOtroPersona.UseVisualStyleBackColor = true;
            // 
            // cbxCarrera
            // 
            this.cbxCarrera.FormattingEnabled = true;
            this.cbxCarrera.Items.AddRange(new object[] {
            "Seleccione..."});
            this.cbxCarrera.Location = new System.Drawing.Point(451, 254);
            this.cbxCarrera.Name = "cbxCarrera";
            this.cbxCarrera.Size = new System.Drawing.Size(315, 24);
            this.cbxCarrera.TabIndex = 63;
            // 
            // cbxCategoria
            // 
            this.cbxCategoria.FormattingEnabled = true;
            this.cbxCategoria.Items.AddRange(new object[] {
            "Seleccione..."});
            this.cbxCategoria.Location = new System.Drawing.Point(451, 326);
            this.cbxCategoria.Name = "cbxCategoria";
            this.cbxCategoria.Size = new System.Drawing.Size(315, 24);
            this.cbxCategoria.TabIndex = 64;
            // 
            // txtJornadaPersona
            // 
            this.txtJornadaPersona.Location = new System.Drawing.Point(451, 388);
            this.txtJornadaPersona.Name = "txtJornadaPersona";
            this.txtJornadaPersona.Size = new System.Drawing.Size(315, 24);
            this.txtJornadaPersona.TabIndex = 65;
            // 
            // btnAgregarCarrera
            // 
            this.btnAgregarCarrera.Location = new System.Drawing.Point(795, 226);
            this.btnAgregarCarrera.Name = "btnAgregarCarrera";
            this.btnAgregarCarrera.Size = new System.Drawing.Size(131, 63);
            this.btnAgregarCarrera.TabIndex = 66;
            this.btnAgregarCarrera.Text = "Agregar Carrera";
            this.btnAgregarCarrera.UseVisualStyleBackColor = true;
            this.btnAgregarCarrera.Click += new System.EventHandler(this.btnAgregarCarrera_Click);
            // 
            // btnAgregarCategoria
            // 
            this.btnAgregarCategoria.Location = new System.Drawing.Point(795, 305);
            this.btnAgregarCategoria.Name = "btnAgregarCategoria";
            this.btnAgregarCategoria.Size = new System.Drawing.Size(131, 65);
            this.btnAgregarCategoria.TabIndex = 67;
            this.btnAgregarCategoria.Text = "Agregar Categoria";
            this.btnAgregarCategoria.UseVisualStyleBackColor = true;
            this.btnAgregarCategoria.Click += new System.EventHandler(this.btnAgregarCategoria_Click);
            // 
            // PantallaMantenedorPersona
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(938, 719);
            this.Controls.Add(this.btnAgregarCategoria);
            this.Controls.Add(this.btnAgregarCarrera);
            this.Controls.Add(this.txtJornadaPersona);
            this.Controls.Add(this.cbxCategoria);
            this.Controls.Add(this.cbxCarrera);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtEmailPersona);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtTelefonoPersona);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtApellidoPersona);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnListarPersona);
            this.Controls.Add(this.btnEditarPersona);
            this.Controls.Add(this.btnEliminarPersona);
            this.Controls.Add(this.dataGridViewPersona);
            this.Controls.Add(this.btnCrearPersona);
            this.Controls.Add(this.txtNombrePersona);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtRutPersona);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "PantallaMantenedorPersona";
            this.Text = "Salas Vivas - Personas";
            this.Load += new System.EventHandler(this.PantallaMantenedorPersona_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPersona)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reservaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem personaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equipoToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnListarPersona;
        private System.Windows.Forms.Button btnEditarPersona;
        private System.Windows.Forms.Button btnEliminarPersona;
        private System.Windows.Forms.DataGridView dataGridViewPersona;
        private System.Windows.Forms.Button btnCrearPersona;
        private System.Windows.Forms.TextBox txtNombrePersona;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRutPersona;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtApellidoPersona;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTelefonoPersona;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtEmailPersona;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbMujerPersona;
        private System.Windows.Forms.RadioButton rbHombrePersona;
        private System.Windows.Forms.RadioButton rbOtroPersona;
        private System.Windows.Forms.ComboBox cbxCarrera;
        private System.Windows.Forms.ComboBox cbxCategoria;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.TextBox txtJornadaPersona;
        private System.Windows.Forms.Button btnAgregarCarrera;
        private System.Windows.Forms.Button btnAgregarCategoria;
    }
}