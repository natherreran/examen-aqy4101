﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDTO;
using CapaNegocio;
using System.Windows.Forms;

namespace CapaConsumoGUI
{
    public partial class PantallaMantenedorCategoria : Form
    {
        public PantallaMantenedorCategoria()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void reservaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionReservaSala pReserva = new PantallaTransaccionReservaSala();
            pReserva.ShowDialog();
        }

        private void salasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorSala pSala = new PantallaMantenedorSala();
            pSala.ShowDialog();
        }

        private void personaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorPersona pPersona = new PantallaMantenedorPersona();
            pPersona.ShowDialog();
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaConsultas pConsultas = new PantallaConsultas();
            pConsultas.ShowDialog();
        }

        private void equipoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaTransaccionEquipo pEquipo = new PantallaTransaccionEquipo();
            pEquipo.ShowDialog();
        }

        private void limpiar()
        {
            this.txtIdCategoria.Text = String.Empty;
            this.txtNombreCategoria.Text = String.Empty;
            this.txtIdCategoria.Focus();
            this.dataGridViewCategoria.DataSource = null;
        }

        private void habilitar()
        {
            this.txtIdCategoria.Enabled = true;
            this.txtNombreCategoria.Enabled = true;
        }

        private void desHabilitar()
        {
            this.txtIdCategoria.Enabled = false;
            this.txtIdCategoria.Enabled = false;
        }


        private void btoCrear_Click(object sender, EventArgs e)
        {
            if (this.txtIdCategoria.Text == "" && this.txtNombreCategoria.Text == "")
            {
                MessageBox.Show("Complete los datos solicitados");
            }
            else
            {
                if (Convert.ToInt32(this.txtIdCategoria.Text) <= 0)
                {
                    MessageBox.Show("El Campo Id Categoria no permite el ingreso de numero negativos");
                }
                else
                {
                    ServiceCategoria.WebServiceCategoriaSoapClient auxNegocioCategoria = new ServiceCategoria.WebServiceCategoriaSoapClient();
                    ServiceCategoria.Categoria auxCategoria = new ServiceCategoria.Categoria();

                    auxCategoria.Id_categoria = Convert.ToInt32(this.txtIdCategoria.Text);
                    auxCategoria.Nombre_categoria = this.txtNombreCategoria.Text;
                    auxNegocioCategoria.grabarCegoriaService(auxCategoria);
                    MessageBox.Show("Datos Guardados Exitosamente", "Sistema");
                    this.limpiar();
                }
            }      
        }

        private void btoEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas Seguro de eliminar ", "sistema", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ServiceCategoria.WebServiceCategoriaSoapClient auxNegocioCategoria = new ServiceCategoria.WebServiceCategoriaSoapClient();
                

                auxNegocioCategoria.eliminarCategoriaService(Convert.ToInt32(this.txtIdCategoria.Text));
                MessageBox.Show("Categoria Eliminado Exitosamente", "Sistema");
            }
        }

        private void btoEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de actualizar ", "sistema", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ServiceCategoria.WebServiceCategoriaSoapClient auxNegocioCategoria = new ServiceCategoria.WebServiceCategoriaSoapClient();
                ServiceCategoria.Categoria auxCategoria = new ServiceCategoria.Categoria();

                auxCategoria.Id_categoria = Convert.ToInt32(this.txtIdCategoria.Text);
                auxCategoria.Nombre_categoria = this.txtNombreCategoria.Text;
                auxNegocioCategoria.actualizarCategoriaService(auxCategoria);
                MessageBox.Show("Datos Actualizados Exitosamente", "Sistema");
                this.limpiar();
            }
                
        }

        private void btoListar_Click(object sender, EventArgs e)
        {
            if(this.btoListar.Text.Equals("Limpiar"))
            {
                limpiar();
                this.btoListar.Text = "Listar";
            }
            else{

                ServiceCategoria.WebServiceCategoriaSoapClient auxNegocioCategoria = new ServiceCategoria.WebServiceCategoriaSoapClient();
                
                this.dataGridViewCategoria.DataSource = auxNegocioCategoria.listarCategoriaService();
                this.dataGridViewCategoria.DataMember = "Categoria";
            }
                
        }
    }
}
