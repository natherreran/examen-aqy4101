﻿using CapaConexion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioConsulta
    {
        private Conexion conec;

        public Conexion Conec { get => conec; set => conec = value; }

        public void configurarConexion()
        {
            this.Conec = new Conexion();
            this.Conec.NombreBaseDatos = "bd_salaVivas";
            //Data Source Naty
            //this.Conec.CadenaConexion = @"Data Source=DESKTOP-Q5141FB\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
            //Data Source Tamy
            this.Conec.CadenaConexion = @"Data Source=DESKTOP-3KO1GI4\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";

        } //Fin configurar

        //TAMY: VOY A DEJAR LOS METODOS A MEDIO HACER, PARA QUE TENGAMOS UNA IDEA NOMAS
        public DataSet MostrarReservasHoy(int dia, int mes, int anno)
        {
            this.configurarConexion();
            this.Conec.NombreTabla = "ReservaSala";
            this.Conec.CadenaSQL = "SELECT * FROM " + this.conec.NombreTabla
                                    + " WHERE MONTH(fecha_reserva) = " + mes
                                    + " AND YEAR(fecha_reserva) = " + anno
                                    + " AND DAY(fecha_reserva) = " + dia + ";";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet;
        }
        public DataSet MostrarReservasMensuales(int mes ,int anno)
        {
            this.configurarConexion();
            this.Conec.NombreTabla = "ReservaSala";
            this.Conec.CadenaSQL = "SELECT * FROM " + this.conec.NombreTabla 
                                    + " WHERE MONTH(fecha_reserva) = " + mes
                                    + " AND YEAR(fecha_reserva) = " + anno
                                    + " ;";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet;
        }
        public DataSet MostrarReservasAnuladas(int dia, int mes, int anno)
        {
            this.configurarConexion();
            this.Conec.NombreTabla = "ReservaSala";
            this.Conec.CadenaSQL = "select COUNT(rsh.codigo_reservaHistorico) - COUNT(rs.codigo_reserva) as 'Cantidad de reservas Anuladas hoy'" 
                + " FROM ReservaSalaHistorico rsh LEFT JOIN ReservaSala rs ON rsh.codigo_reservaHistorico = rs.codigo_reserva"
                + " WHERE DAY(rsh.fecha_reserva) = " + dia
                + " AND MONTH(rsh.fecha_reserva) = " + mes
                + " AND YEAR(rsh.fecha_reserva) = " + anno
                + "; ";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet;
        }
        
        public DataSet MostrarCantidadEquipos()
        {
            this.configurarConexion();
            this.Conec.NombreTabla = "equipo";
            this.Conec.CadenaSQL = "SELECT COUNT(cod_equipo) as 'Equipos inscritos hasta hoy' FROM " + this.conec.NombreTabla + ";";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet;
        }

        
    }
}
