﻿using CapaConexion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDTO;
using System.Data;

namespace CapaNegocio
{
    public class NegocioCarrera
    {
        private Conexion conec;

        public Conexion Conec { get => conec; set => conec = value; }

        public void configurarConexion()
        {
            this.Conec = new Conexion();
            this.Conec.NombreBaseDatos = "bd_salaVivas";
            this.Conec.NombreTabla = "Carrera";
            //Data Source Naty
            //this.Conec.CadenaConexion = @"Data Source=DESKTOP-Q5141FB\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
            //Data Source Tamy
            this.Conec.CadenaConexion = @"Data Source=DESKTOP-3KO1GI4\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";

        } //Fin configurar

        public void grabarCarrera(Carrera carrera)
        {
            this.configurarConexion();

            this.Conec.CadenaSQL = "INSERT INTO " + this.Conec.NombreTabla
                                   + " (id_carrera, nombre_carrera) VALUES ('" + carrera.Id_carrera
                                   + "','" + carrera.Nombre_carrera + "');";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void actualizarCarrera(Carrera carrera)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "UPDATE " + this.Conec.NombreTabla
                                   + " SET"
                                   + " nombre_carrera ='" + carrera.Nombre_carrera
                                   + "' WHERE id_carrera = " + carrera.Id_carrera + " ;";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }
       
        public void eliminarCarrera(int id_carrera)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "DELETE FROM " + this.Conec.NombreTabla
                                   + " WHERE id_carrera = '" + id_carrera + "';";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public DataSet listarCarrera()
        {
            this.configurarConexion();

            this.Conec.CadenaSQL = "SELECT * FROM carrera";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet;
        }

        public Carrera buscarCarrera(int id_carrera)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "SELECT * FROM carrera"
                                   + " WHERE id_carrera = " + id_carrera + ";";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            Carrera auxCarrera = new Carrera();
            DataTable dt = new DataTable();
            dt = this.Conec.DbDataSet.Tables[this.Conec.NombreTabla];

            try
            {
                auxCarrera.Id_carrera = (int)dt.Rows[0]["id_carrera"];
                auxCarrera.Nombre_carrera = (String)dt.Rows[0]["nombre_carrera"];
            }
            catch (Exception ex)
            {
                auxCarrera.Id_carrera = 0;
                auxCarrera.Nombre_carrera = String.Empty;

            }

            return auxCarrera;
        } //Fin buscar

        
    }

}
