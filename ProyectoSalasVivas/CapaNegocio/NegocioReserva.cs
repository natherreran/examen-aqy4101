﻿using CapaConexion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDTO;
using System.Data;

namespace CapaNegocio
{
    public class NegocioReserva
    {
        private Conexion conec;

        public Conexion Conec { get => conec; set => conec = value; }

        public void configurarConexion()
        {
            this.Conec = new Conexion();
            this.Conec.NombreBaseDatos = "bd_salaVivas";
            this.Conec.NombreTabla = "ReservaSala";
            //Data Source Naty
            //this.Conec.CadenaConexion = @"Data Source=DESKTOP-Q5141FB\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
            //Data Source Tamy
            this.Conec.CadenaConexion = @"Data Source=DESKTOP-3KO1GI4\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
        } //Fin configurar

        public void grabarReservaSala(ReservarSala reservarSala)
        {
            this.configurarConexion();

            this.Conec.CadenaSQL = "INSERT INTO ReservaSala"
                                   + " (codigo_reserva, observacion_reserva, fecha_reserva, hora_inicio, hora_termino,id_sala, cod_equipo) VALUES ("
                                   + reservarSala.Codigo_reserva
                                   + ", '" + reservarSala.Observacion_reserva 
                                   + "' ,'" + reservarSala.Fecha_reserva
                                   + "', '" + reservarSala.Hora_inicio_reserva
                                   + "', '" + reservarSala.Hora_termino_reserva
                                   + "'," + reservarSala.Id_sala
                                   + "," + reservarSala.Cod_equipo + ");";
            
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void grabarReservaSalaHistorico(ReservarSala reservarSala)
        {
            this.configurarConexion();

            this.Conec.CadenaSQL = "INSERT INTO ReservaSalaHistorico"
                                   + " (codigo_reservaHistorico, observacion_reserva, fecha_reserva, hora_inicio, hora_termino, id_sala, cod_equipo) VALUES ("
                                   + reservarSala.Codigo_reserva
                                   + ", '" + reservarSala.Observacion_reserva
                                   + "' ,'" + reservarSala.Fecha_reserva
                                   + "', '" + reservarSala.Hora_inicio_reserva
                                   + "', '" + reservarSala.Hora_termino_reserva
                                   + "'," + reservarSala.Id_sala
                                   + "," + reservarSala.Cod_equipo + ");";

            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void actualizarReservarSala(ReservarSala reservarSala)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "UPDATE " + this.Conec.NombreTabla
                                   + " SET" 
                                   + " observacion_reserva = '" + reservarSala.Observacion_reserva
                                   + "', fecha_reserva = '" + reservarSala.Fecha_reserva
                                   + "', hora_inicio = '" + reservarSala.Hora_inicio_reserva
                                   + "', hora_termino = '" + reservarSala.Hora_termino_reserva
                                   + "', id_sala = " + reservarSala.Id_sala
                                   + ", codigo_equipo = " + reservarSala.Cod_equipo
                                   + " WHERE codigo_reserva= '" + reservarSala.Codigo_reserva + "';";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void anularReservarSala(int codigo_reserva)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "DELETE FROM " + this.Conec.NombreTabla
                                   + " WHERE codigo_reserva = '" + codigo_reserva + "';";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public DataSet listarReservarSala()
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "SELECT * FROM " + this.Conec.NombreTabla;
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet;
        }


        public DataTable MostrarNombreSala()
        {
            this.configurarConexion();
            this.Conec.NombreTabla = "Sala";
            this.Conec.CadenaSQL = "SELECT * FROM " + this.conec.NombreTabla;
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet.Tables[this.Conec.NombreTabla];
        }

        public DataTable MostrarCodEquipo()
        {
            this.configurarConexion();
            this.Conec.NombreTabla = "Equipo";
            this.Conec.CadenaSQL = "SELECT * FROM " + this.conec.NombreTabla;
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet.Tables[this.Conec.NombreTabla];
        }


        public ReservarSala buscarReservarSala(int codigo_reserva)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "SELECT * FROM " + this.Conec.NombreTabla
                                   + " WHERE codigo_reserva = " + codigo_reserva + " ;";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            ReservarSala auxReservaSala = new ReservarSala();
            DataTable dt = new DataTable();
            dt = this.Conec.DbDataSet.Tables[this.Conec.NombreTabla];

            try
            {
                auxReservaSala.Codigo_reserva = (int)dt.Rows[0]["codigo_reserva"];
                auxReservaSala.Observacion_reserva = (String)dt.Rows[0]["observacion_reserva"];
                auxReservaSala.Fecha_reserva = (DateTime)dt.Rows[0]["fecha_reserva"];
                auxReservaSala.Hora_inicio_reserva = (String)dt.Rows[0]["hora_inicio"];
                auxReservaSala.Hora_termino_reserva = (String)dt.Rows[0]["hora_termino"];
                auxReservaSala.Id_sala = (int)dt.Rows[0]["id_sala"];
                auxReservaSala.Cod_equipo = (int)dt.Rows[0]["cod_equipo"];

            }
            catch (Exception ex)
            {

                auxReservaSala.Codigo_reserva = 0;
                auxReservaSala.Observacion_reserva = String.Empty;
                auxReservaSala.Fecha_reserva = DateTime.Today;
                auxReservaSala.Hora_inicio_reserva = String.Empty;
                auxReservaSala.Hora_termino_reserva = String.Empty;
                auxReservaSala.Id_sala = 0;
                auxReservaSala.Cod_equipo = 0;
            }

            return auxReservaSala;
        } //Fin buscar
    }
}
