﻿using CapaConexion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaDTO;
using System.Threading.Tasks;
using System.Data;

namespace CapaNegocio
{
    public class NegocioPersona
    {
        private Conexion conec;

        public Conexion Conec { get => conec; set => conec = value; }

        public void configurarConexion()
        {
            this.Conec = new Conexion();
            this.Conec.NombreBaseDatos = "bd_salaVivas";
            this.Conec.NombreTabla = "Persona";
            //Data Source Naty
            //this.Conec.CadenaConexion = @"Data Source=DESKTOP-Q5141FB\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
            //Data Source Tamy
            this.Conec.CadenaConexion = @"Data Source=DESKTOP-3KO1GI4\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
        } //Fin configurar

        public void grabarPersona(Persona persona)
        {
            this.configurarConexion();

            this.Conec.CadenaSQL = "INSERT INTO " + this.Conec.NombreTabla
                                   + " (rut_persona, nombre_persona, apellido_persona, sexo, telefono, email, id_categoria, id_carrera, jornada) VALUES ('" 
                                   + persona.Rut_persona
                                   + "','" + persona.Nombre_persona
                                   + "','" + persona.Apellido_persona
                                   + "','" + persona.Sexo
                                   + "','" + persona.Telefono
                                   + "','" + persona.Email
                                   + "', " + persona.Id_categoria
                                   + " , " + persona.Id_carrera
                                   + " ,'" + persona.Jornada + "');";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void actualizarPersona(Persona persona)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "UPDATE " + this.Conec.NombreTabla
                                   + " SET"
                                   + " nombre_persona ='" + persona.Nombre_persona
                                   + "', apellido_persona = '" + persona.Apellido_persona
                                   + "', sexo= '" + persona.Sexo
                                   + "', telefono = " + persona.Telefono
                                   + ", email = '" + persona.Email
                                   + "', id_categoria = " + persona.Id_categoria
                                   + ", id_carrera= " + persona.Id_carrera
                                   + ", jornada = '" + persona.Jornada
                                   + "' WHERE rut_persona= '" + persona.Rut_persona + "';";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void eliminarPersona(String rut_persona)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "DELETE FROM " + this.Conec.NombreTabla
                                   + " WHERE rut_persona = '" + rut_persona + "';";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public DataSet listarPersona()
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "SELECT * FROM " + this.conec.NombreTabla ;
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet;
        }


        public DataTable MostrarNombreCategoria()
        {
            this.configurarConexion();
            this.Conec.NombreTabla = "Categoria";
            this.Conec.CadenaSQL = "SELECT * FROM " + this.conec.NombreTabla;
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet.Tables[this.Conec.NombreTabla];
            
            
        }

        public DataTable MostrarNombreCarrera()
        {
            this.configurarConexion();
            this.Conec.NombreTabla = "Carrera";
            this.Conec.CadenaSQL = "SELECT * FROM " + this.conec.NombreTabla;
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet.Tables[this.Conec.NombreTabla];


        }

       
        public Persona buscarPersona(String rut_persona)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "SELECT * FROM persona"
                                   + " WHERE rut_persona = '" + rut_persona + "';";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            Persona auxPersona = new Persona();
            DataTable dt = new DataTable();
            dt = this.Conec.DbDataSet.Tables[this.Conec.NombreTabla];

            try
            {
                auxPersona.Rut_persona = (String)dt.Rows[0]["rut_persona"];
                auxPersona.Nombre_persona = (String)dt.Rows[0]["nombre_persona"];
                auxPersona.Apellido_persona = (String)dt.Rows[0]["apellido_persona"];
                auxPersona.Sexo = (String)dt.Rows[0]["sexo"];
                auxPersona.Telefono = (Int32)dt.Rows[0]["telefono"];
                auxPersona.Email = (String)dt.Rows[0]["email"];
                auxPersona.Id_categoria = (Int32)dt.Rows[0]["id_categoria"];
                auxPersona.Id_carrera = (Int32)dt.Rows[0]["id_carrera"];
                auxPersona.Jornada = (String)dt.Rows[0]["jornada"];


            }
            catch (Exception ex)
            {
                
                auxPersona.Rut_persona = String.Empty;
                auxPersona.Nombre_persona = String.Empty;
                auxPersona.Apellido_persona = String.Empty;
                auxPersona.Sexo = String.Empty;
                auxPersona.Telefono = 0;
                auxPersona.Email = String.Empty;
                auxPersona.Id_categoria = 0;
                auxPersona.Id_carrera = 0; 
                auxPersona.Jornada = String.Empty; 
            }

            return auxPersona;
        } //Fin buscar
    }
}
