﻿using CapaConexion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaDTO;
using System.Threading.Tasks;
using System.Data;

namespace CapaNegocio
{
    public class NegocioCategoria
    {
        private Conexion conec;

        public Conexion Conec { get => conec; set => conec = value; }

        public void configurarConexion()
        {
            this.Conec = new Conexion();
            this.Conec.NombreBaseDatos = "bd_salaVivas";
            this.Conec.NombreTabla = "Categoria";
            //Data Source Naty
            //this.Conec.CadenaConexion = @"Data Source=DESKTOP-Q5141FB\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
            //Data Source Tamy
            this.Conec.CadenaConexion = @"Data Source=DESKTOP-3KO1GI4\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
        } //Fin configurar

        public void grabarCategoria(Categoria categoria)
        {
            this.configurarConexion();

            this.Conec.CadenaSQL = "INSERT INTO " + this.Conec.NombreTabla
                                   + " (id_categoria, nombre_categoria) VALUES ('" + categoria.Id_categoria
                                   + "','" + categoria.Nombre_categoria + "');";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void actualizarCategoria(Categoria categoria)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "UPDATE " + this.Conec.NombreTabla
                                   + " SET"
                                   + " nombre_categoria ='" + categoria.Nombre_categoria
                                   + "' WHERE id_categoria= " + categoria.Id_categoria + ";";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void eliminarCategoria(int id_categoria)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "DELETE FROM " + this.Conec.NombreTabla
                                   + " WHERE id_categoria = '" + id_categoria + "';";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public DataSet listarCategoria()
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "SELECT * FROM Categoria";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet;
        }

        public Categoria buscarCategoria(int id_categoria)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "SELECT * FROM Categoria"
                                   + " WHERE id_categoria = '" + id_categoria + "';";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            Categoria auxCategoria = new Categoria();
            DataTable dt = new DataTable();
            dt = this.Conec.DbDataSet.Tables[this.Conec.NombreTabla];

            try
            {
                auxCategoria.Id_categoria = (int)dt.Rows[0]["id_categoria"];
                auxCategoria.Nombre_categoria = (String)dt.Rows[0]["nombre_categoria"];
                

            }
            catch (Exception ex)
            {
                auxCategoria.Id_categoria = 0;
                auxCategoria.Nombre_categoria = String.Empty;

            }

            return auxCategoria;
        } //Fin buscar
    }
}
