﻿using CapaConexion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDTO;
using System.Data;

namespace CapaNegocio
{
    public class NegocioSala
    {
        private Conexion conec;

        public Conexion Conec { get => conec; set => conec = value; }

        public void configurarConexion()
        {
            this.Conec = new Conexion();
            this.Conec.NombreBaseDatos = "bd_salaVivas";
            this.Conec.NombreTabla = "Sala";
            //Data Source Naty
            //this.Conec.CadenaConexion = @"Data Source=DESKTOP-Q5141FB\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
            //Data Source Tamy
            this.Conec.CadenaConexion = @"Data Source=DESKTOP-3KO1GI4\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
        } //Fin configurar   

        public void grabarSala(Sala sala)
        {
            this.configurarConexion();

            this.Conec.CadenaSQL = "INSERT INTO " + this.Conec.NombreTabla
                                   + " (id_sala, nombre_sala, descripcion_sala, cantidad_integrante) VALUES ('" 
                                   + sala.Id_sala
                                   + "','"
                                   + sala.Nombre_sala
                                   + "','"
                                   + sala.Descripcion_sala 
                                   + "','"
                                   + sala.Cantidad_integrante + "');";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void actualizarSala(Sala sala)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "UPDATE " + this.Conec.NombreTabla
                                   + " SET"
                                   + " nombre_sala ='" + sala.Nombre_sala
                                   + "', descripcion_sala ='" + sala.Descripcion_sala
                                   + "', cantidad_integrante = " + sala.Cantidad_integrante
                                   + " WHERE id_sala= " + sala.Id_sala + " ;";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void eliminarSala(int id_sala)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "DELETE FROM " + this.Conec.NombreTabla
                                   + " WHERE id_sala = '" + id_sala + "';";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public DataSet listarSala()
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "SELECT * FROM " + this.Conec.NombreTabla;
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet;
        }

        public Sala buscarSala(int id_sala)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "SELECT * FROM " + this.Conec.NombreTabla
                                   + " WHERE id_sala = '" + id_sala + "';";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            Sala auxSala = new Sala();
            DataTable dt = new DataTable();
            dt = this.Conec.DbDataSet.Tables[this.Conec.NombreTabla];

            try
            {
                auxSala.Id_sala = (int)dt.Rows[0]["id_sala"];
                auxSala.Nombre_sala = (String)dt.Rows[0]["nombre_sala"];
                auxSala.Descripcion_sala = (String)dt.Rows[0]["descripcion_sala"];
                auxSala.Cantidad_integrante = (int)dt.Rows[0]["cantidad_integrante"];

            }
            catch (Exception ex)
            {
                auxSala.Id_sala = 0;
                auxSala.Descripcion_sala = String.Empty;
                auxSala.Cantidad_integrante = 0;

            }

            return auxSala;
        } //Fin buscar
    }
}
