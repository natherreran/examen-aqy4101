﻿using CapaConexion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaDTO;
using System.Threading.Tasks;
using System.Data;

namespace CapaNegocio
{
    public class NegocioEquipo
    {
        private Conexion conec;

        public Conexion Conec { get => conec; set => conec = value; }

        public void configurarConexion()
        {
            this.Conec = new Conexion();
            this.Conec.NombreBaseDatos = "bd_salaVivas";
            this.Conec.NombreTabla = "Equipo";
            //Data Source Naty
            //this.Conec.CadenaConexion = @"Data Source=DESKTOP-Q5141FB\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
            //Data Source Tamy
            this.Conec.CadenaConexion = @"Data Source=DESKTOP-3KO1GI4\SQLEXPRESS;Initial Catalog=bd_salaVivas;Integrated Security=True";
        } //Fin configurar

        public void grabarEquipo(Equipo equipo)
        {
            this.configurarConexion();

            this.Conec.CadenaSQL = "INSERT INTO " + this.Conec.NombreTabla
                                   + " (cod_equipo, es_docente, es_encargado, cantidad, rut_persona) VALUES (" 
                                   + equipo.Cod_equipo
                                   + " , " + equipo.Es_docente
                                   + " , " + equipo.Es_encargado
                                   + " , " + equipo.Cantidad
                                   + " , '" + equipo.Rut_persona + "' );";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void actualizarEquipo(Equipo equipo)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "UPDATE " + this.Conec.NombreTabla
                                   + " SET"
                                   + " es_docente = " + equipo.Es_docente
                                   + " ,es_encargado = " + equipo.Es_encargado
                                   + " ,cantidad = " + equipo.Cantidad
                                   + " ,rut_persona = '" + equipo.Rut_persona
                                   + "' WHERE cod_equipo= " + equipo.Cod_equipo + ";";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public void eliminarEquipo(int cod_equipo)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "DELETE FROM " + this.Conec.NombreTabla
                                   + " WHERE cod_equipo = '" + cod_equipo + "';";
            this.Conec.EsSelect = false;
            this.Conec.conectar();
        }

        public DataSet listarEquipo()
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "SELECT * FROM equipo";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            return this.Conec.DbDataSet;
        }

        public Equipo buscarEquipo(int cod_equipo)
        {
            this.configurarConexion();
            this.Conec.CadenaSQL = "SELECT * FROM equipo"
                                   + " WHERE cod_equipo = '" + cod_equipo + "';";
            this.Conec.EsSelect = true;
            this.Conec.conectar();
            Equipo auxEquipo = new Equipo();
            DataTable dt = new DataTable();
            dt = this.Conec.DbDataSet.Tables[this.Conec.NombreTabla];

            try
            {
                auxEquipo.Cod_equipo = (Int32)dt.Rows[0]["cod_equipo"];
                auxEquipo.Es_docente = (Int32)dt.Rows[0]["es_docente"];
                auxEquipo.Es_encargado = (Int32)dt.Rows[0]["es_encargado"];
                auxEquipo.Cantidad = (Int32)dt.Rows[0]["cantidad"];
                auxEquipo.Rut_persona = (String)dt.Rows[0]["rut_persona"];

            }
            catch (Exception ex)
            {
                auxEquipo.Cod_equipo = 0;
                auxEquipo.Es_docente = 0;
                auxEquipo.Es_encargado = 0;
                auxEquipo.Cantidad = 0;
                auxEquipo.Rut_persona = String.Empty; ;

            }

            return auxEquipo;
        } //Fin buscar
    }
}
