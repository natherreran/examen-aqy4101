﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaConexion
{
    public class Conexion
    {
        private String nombreBaseDatos;
        private String nombreTabla;
        private String cadenaConexion;
        private String cadenaSQL;
        private Boolean esSelect;
        private SqlConnection dbConnection;
        private SqlDataAdapter dbDataAdapter;
        private DataSet dbDataSet;

        public string NombreBaseDatos { get => nombreBaseDatos; set => nombreBaseDatos = value; }
        public string NombreTabla { get => nombreTabla; set => nombreTabla = value; }
        public string CadenaConexion { get => cadenaConexion; set => cadenaConexion = value; }
        public string CadenaSQL { get => cadenaSQL; set => cadenaSQL = value; }
        public bool EsSelect { get => esSelect; set => esSelect = value; }
        public SqlConnection DbConnection { get => dbConnection; set => dbConnection = value; }
        public SqlDataAdapter DbDataAdapter { get => dbDataAdapter; set => dbDataAdapter = value; }
        public DataSet DbDataSet { get => dbDataSet; set => dbDataSet = value; }

        public void abrir()
        {
            try
            {
                this.dbConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al abrir conexion " + ex.Message, "Sistema");

            }

        } //Fin abrir

        public void cerrar()
        {
            try
            {
                this.dbConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al cerrar conexion " + ex.Message, "Sistema");

            }

        } //Fin Cerrar


        public void conectar()
        {
            if (this.NombreBaseDatos.Length == 0)
            {
                MessageBox.Show("Falta Nombre Base de Datos ", "Sistema");
                return;
            }

            if (this.NombreTabla.Length == 0)
            {
                MessageBox.Show("Falta Nombre Tabla ", "Sistema");
                return;
            }

            if (this.CadenaConexion.Length == 0)
            {
                MessageBox.Show("Falta Cadena Conexion ", "Sistema");
                return;
            }

            if (this.CadenaSQL.Length == 0)
            {
                MessageBox.Show("Falta cadena SQL ", "Sistema");
                return;
            }

            //Se instancia la conexion

            try
            {
                this.DbConnection = new SqlConnection(this.CadenaConexion);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de conexion " + ex.Message, "Sistema");
                return;
            }

            this.abrir();

            if (this.EsSelect) //SELECT
            {
                //Se instancia DataSet
                this.DbDataSet = new DataSet();
                try
                {
                    this.DbDataAdapter = new SqlDataAdapter(this.CadenaSQL, this.DbConnection);
                    this.DbDataAdapter.Fill(this.DbDataSet, this.NombreTabla);
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Error al cargar DataSet " + ex.Message, "Sistema");
                    return;
                }


            }
            else //INSERT UPDATE DELETE
            {
                try
                {
                    SqlCommand variableSQL = new SqlCommand(this.CadenaSQL, this.DbConnection);
                    variableSQL.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Error SQL " + ex.Message, "Sistema");
                    return;
                }

            }

            this.cerrar();



        } //fin conectar

        
    }
}
