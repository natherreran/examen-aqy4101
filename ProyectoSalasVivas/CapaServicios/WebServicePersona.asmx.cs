﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaDTO;
using CapaNegocio;
using System.Web.Services;
using System.Data;

namespace CapaServicios
{
    /// <summary>
    /// Descripción breve de WebServicePersona
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServicePersona : System.Web.Services.WebService
    {

        [WebMethod]
        public void grabarPersonaService(Persona persona)
        {
            NegocioPersona auxNegocioPersona = new NegocioPersona();
            auxNegocioPersona.grabarPersona(persona);
        }

        [WebMethod]
        public void actualizarPersonaService(Persona persona)
        {
            NegocioPersona auxNegocioPersona = new NegocioPersona();
            auxNegocioPersona.actualizarPersona(persona);
        }

        [WebMethod]
        public void eliminarPersonaService(String rut_persona)
        {
            NegocioPersona auxNegocioPersona = new NegocioPersona();
            auxNegocioPersona.eliminarPersona(rut_persona);
        }

        [WebMethod]
        public DataSet listarPersonaService()
        {
            NegocioPersona auxNegocioPersona = new NegocioPersona();
            return auxNegocioPersona.listarPersona();
        }

        [WebMethod]
        public DataTable MostrarNombreCategoriaService()
        {
            NegocioPersona auxNegocioPersona = new NegocioPersona();
             return auxNegocioPersona.MostrarNombreCarrera();
        }

        [WebMethod]
        public DataTable MostrarNombreCarreraService()
        {
            NegocioPersona auxNegocioPersona = new NegocioPersona();
            return auxNegocioPersona.MostrarNombreCategoria();
        }

        [WebMethod]
        public Persona buscarPersona(String rut_persona)
        {
            NegocioPersona auxNegocioPersona = new NegocioPersona();
            return auxNegocioPersona.buscarPersona(rut_persona);
        }
    }
}
