﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaDTO;
using CapaNegocio;
using System.Web.Services;
using System.Data;

namespace CapaServicios
{
    /// <summary>
    /// Descripción breve de WebServiceEquipo
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceEquipo : System.Web.Services.WebService
    {

        [WebMethod]
        public void grabarEquipoService(Equipo equipo)
        {
            NegocioEquipo auxNegocioEquipo = new NegocioEquipo();
            auxNegocioEquipo.grabarEquipo(equipo);
        }

        [WebMethod]
        public void actualizarEquipoService(Equipo equipo)
        {
            NegocioEquipo auxNegocioEquipo = new NegocioEquipo();
            auxNegocioEquipo.actualizarEquipo(equipo);
        }

        [WebMethod]
        public void eliminarEquipoService(int cod_equipo)
        {
            NegocioEquipo auxNegocioEquipo = new NegocioEquipo();
            auxNegocioEquipo.eliminarEquipo(cod_equipo);
        }

        [WebMethod]
        public DataSet listarEquipoService()
        {
            NegocioEquipo auxNegocioEquipo = new NegocioEquipo();
            return auxNegocioEquipo.listarEquipo();
        }

        [WebMethod]
        public Equipo buscarEquipo(int cod_equipo)
        {
            NegocioEquipo auxNegocioEquipo = new NegocioEquipo();
            return auxNegocioEquipo.buscarEquipo(cod_equipo);
        }
    }
}
