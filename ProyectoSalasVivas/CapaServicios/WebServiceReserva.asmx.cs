﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaDTO;
using CapaNegocio;
using System.Web.Services;
using System.Data;

namespace CapaServicios
{
    /// <summary>
    /// Descripción breve de WebServiceReserva
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceReserva : System.Web.Services.WebService
    {

        [WebMethod]
        public void grabarReservaSalaService(ReservarSala reservarSala)
        {
            NegocioReserva auxNegocioReserva = new NegocioReserva();
            auxNegocioReserva.grabarReservaSala(reservarSala);
        }

        [WebMethod]
        public void grabarReservaSalaHistoricoService(ReservarSala reservarSala)
        {
            NegocioReserva auxNegocioReserva = new NegocioReserva();
            auxNegocioReserva.grabarReservaSalaHistorico(reservarSala);
        }

        [WebMethod]
        public void actualizarReservarSalaService(ReservarSala reservarSala)
        {
            NegocioReserva auxNegocioReserva = new NegocioReserva();
            auxNegocioReserva.actualizarReservarSala(reservarSala);
        }

        [WebMethod]
        public void anularReservarSalaService(int codigo_reserva)
        {
            NegocioReserva auxNegocioReserva = new NegocioReserva();
            auxNegocioReserva.anularReservarSala(codigo_reserva);
        }

        [WebMethod]
        public DataSet listarReservarSalaService()
        {
            NegocioReserva auxNegocioReserva = new NegocioReserva();
            return auxNegocioReserva.listarReservarSala();
        }

        [WebMethod]
        public DataTable MostrarNombreSalaService()
        {
            NegocioReserva auxNegocioReserva = new NegocioReserva();
            return auxNegocioReserva.MostrarNombreSala();
        }

        [WebMethod]
        public DataTable MostrarCodEquipoService()
        {
            NegocioReserva auxNegocioReserva = new NegocioReserva();
            return auxNegocioReserva.MostrarCodEquipo();
        }

        [WebMethod]
        public ReservarSala buscarReservarSalaService(int codogo_reserva)
        {
            NegocioReserva auxNegocioReserva = new NegocioReserva();
            return auxNegocioReserva.buscarReservarSala(codogo_reserva);
        }
    }
}
