﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaDTO;
using CapaNegocio;
using System.Web.Services;
using System.Data;

namespace CapaServicios
{
    /// <summary>
    /// Descripción breve de WebServiceCategoria
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceCategoria : System.Web.Services.WebService
    {

        [WebMethod]
        public void grabarCegoriaService(Categoria categoria)
        {
            NegocioCategoria auxNegocioCategoria = new NegocioCategoria();
            auxNegocioCategoria.grabarCategoria(categoria);
        }

        [WebMethod]
        public void actualizarCategoriaService(Categoria categoria)
        {
            NegocioCategoria auxNegocioCategoria = new NegocioCategoria();
            auxNegocioCategoria.actualizarCategoria(categoria);
        }

        [WebMethod]
        public void eliminarCategoriaService(int id_categoria)
        {
            NegocioCategoria auxNegocioCategoria = new NegocioCategoria();
            auxNegocioCategoria.eliminarCategoria(id_categoria);
        }

        [WebMethod]
        public DataSet listarCategoriaService()
        {
            NegocioCategoria auxNegocioCategoria = new NegocioCategoria();
            return auxNegocioCategoria.listarCategoria();
        }

        [WebMethod]
        public Categoria buscarCategoria(int id_categoria)
        {
            NegocioCategoria auxNegocioCategoria = new NegocioCategoria();
            return auxNegocioCategoria.buscarCategoria(id_categoria);
        }
    }
}
