﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaDTO;
using CapaNegocio;
using System.Web.Services;
using System.Data;

namespace CapaServicios
{
    /// <summary>
    /// Descripción breve de WebServiceSala
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceSala : System.Web.Services.WebService
    {

        [WebMethod]
        public void grabarSalaService(Sala sala)
        {
            NegocioSala auxNegocioSala = new NegocioSala();
            auxNegocioSala.grabarSala(sala);
        }

        [WebMethod]
        public void actualizarSalaService(Sala sala)
        {
            NegocioSala auxNegocioSala = new NegocioSala();
            auxNegocioSala.actualizarSala(sala);
        }

        [WebMethod]
        public void eliminarSalaService(int id_sala)
        {
            NegocioSala auxNegocioSala = new NegocioSala();
            auxNegocioSala.eliminarSala(id_sala);
        }

        [WebMethod]
        public DataSet listarSalaService()
        {
            NegocioSala auxNegocioSala = new NegocioSala();
            return auxNegocioSala.listarSala();
        }

        [WebMethod]
        public Sala buscarSalaService(int id_sala)
        {
            NegocioSala auxNegocioSala = new NegocioSala();
            return auxNegocioSala.buscarSala(id_sala);
        } 
    }
}
