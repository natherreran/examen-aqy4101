﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using CapaDTO;
using CapaNegocio;
using System.Web.Services;

namespace CapaServicios
{
    /// <summary>
    /// Descripción breve de WebServiceConsulta
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceConsulta : System.Web.Services.WebService
    {

        [WebMethod]
        public DataSet MostrarReservasHoyService(int dia, int mes, int anno)
        {
            NegocioConsulta auxNegocioConsulta = new NegocioConsulta();
            return auxNegocioConsulta.MostrarReservasHoy(dia, mes, anno);
        }

        [WebMethod]
        public DataSet MostrarReservasMensualesService(int mes, int anno)
        {
            NegocioConsulta auxNegocioConsulta = new NegocioConsulta();
            return auxNegocioConsulta.MostrarReservasMensuales(mes, anno);
        }

        [WebMethod]
        public DataSet MostrarReservasAnuladasService(int dia, int mes, int anno)
        {
            NegocioConsulta auxNegocioConsulta = new NegocioConsulta();
            return auxNegocioConsulta.MostrarReservasAnuladas(dia, mes, anno);
        }

        [WebMethod]
        public DataSet MostrarCantidadEquiposService()
        {
            NegocioConsulta auxNegocioConsulta = new NegocioConsulta();
            return auxNegocioConsulta.MostrarCantidadEquipos();
        }
    }
}
