﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaDTO;
using CapaNegocio;
using System.Web.Services;
using System.Data;

namespace CapaServicios
{
    /// <summary>
    /// Descripción breve de WebServiceCarrera
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceCarrera : System.Web.Services.WebService
    {

        [WebMethod]
        public void grabarCarreraService(Carrera carrera)
        {
            NegocioCarrera auxNegocioCarrera = new NegocioCarrera();
            auxNegocioCarrera.grabarCarrera(carrera);
        }

        [WebMethod]
        public void actualizarCarreraService(Carrera carrera)
        {
            NegocioCarrera auxNegocioCarrera = new NegocioCarrera();
            auxNegocioCarrera.actualizarCarrera(carrera);
        }

        [WebMethod]
        public void eliminarCarreraService(int id_carrera)
        {
            NegocioCarrera auxNegocioCarrera = new NegocioCarrera();
            auxNegocioCarrera.eliminarCarrera(id_carrera);
        }

        [WebMethod]
        public DataSet listarCarreraService()
        {
            NegocioCarrera auxNegocioCarrera = new NegocioCarrera();
            return auxNegocioCarrera.listarCarrera();
        }

        [WebMethod]
        public Carrera buscarCarreraService(int id_carrera)
        {
            NegocioCarrera auxNegocioCarrera = new NegocioCarrera();
            return auxNegocioCarrera.buscarCarrera(id_carrera);
        } 
        
    }
}
