﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Persona
    {
        private string rut_persona;
        private string nombre_persona;
        private string apellido_persona;
        private string sexo;
        private int telefono;
        private string email;
        private int id_categoria;
        private int id_carrera;
        private string jornada;

        public string Rut_persona { get => rut_persona; set => rut_persona = value; }
        public string Nombre_persona { get => nombre_persona; set => nombre_persona = value; }
        public string Apellido_persona { get => apellido_persona; set => apellido_persona = value; }
        public string Sexo { get => sexo; set => sexo = value; }
        public int Telefono { get => telefono; set => telefono = value; }
        public string Email { get => email; set => email = value; }
        public int Id_categoria { get => id_categoria; set => id_categoria = value; }
        public int Id_carrera { get => id_carrera; set => id_carrera = value; }
        public string Jornada { get => jornada; set => jornada = value; }
    }
}
