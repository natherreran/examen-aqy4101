﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Sala
    {
        private int id_sala;
        private String nombre_sala;
        private string descripcion_sala;
        private int cantidad_integrante;

        public int Id_sala { get => id_sala; set => id_sala = value; }
        public string Descripcion_sala { get => descripcion_sala; set => descripcion_sala = value; }
        public int Cantidad_integrante { get => cantidad_integrante; set => cantidad_integrante = value; }
        public string Nombre_sala { get => nombre_sala; set => nombre_sala = value; }
    }
}
