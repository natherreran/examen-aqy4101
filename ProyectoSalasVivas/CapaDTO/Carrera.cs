﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Carrera
    {
        private int id_carrera;
        private string nombre_carrera;

        public int Id_carrera { get => id_carrera; set => id_carrera = value; }
        public string Nombre_carrera { get => nombre_carrera; set => nombre_carrera = value; }
    }
}

