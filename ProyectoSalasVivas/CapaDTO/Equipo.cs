﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Equipo
    {
        private int cod_equipo;
        private int es_docente;
        private int es_encargado;
        private int cantidad;
        private string rut_persona;

        public int Cod_equipo { get => cod_equipo; set => cod_equipo = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public string Rut_persona { get => rut_persona; set => rut_persona = value; }
        public int Es_docente { get => es_docente; set => es_docente = value; }
        public int Es_encargado { get => es_encargado; set => es_encargado = value; }
    }
}
