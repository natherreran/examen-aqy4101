﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class ReservarSala 
    {
        private int codigo_reserva;
        private string hora_inicio_reserva;
        private string hora_termino_reserva;
        private String observacion_reserva;
        private DateTime fecha_reserva;
        private int id_sala;
        private int cod_equipo;

        public int Codigo_reserva { get => codigo_reserva; set => codigo_reserva = value; }
        public string Hora_inicio_reserva { get => hora_inicio_reserva; set => hora_inicio_reserva = value; }
        public string Hora_termino_reserva { get => hora_termino_reserva; set => hora_termino_reserva = value; }
        public string Observacion_reserva { get => observacion_reserva; set => observacion_reserva = value; }
        public DateTime Fecha_reserva { get => fecha_reserva; set => fecha_reserva = value; }
        public int Id_sala { get => id_sala; set => id_sala = value; }
        public int Cod_equipo { get => cod_equipo; set => cod_equipo = value; }
    }
}
